import { CapacitorConfig } from '@capacitor/cli';

const config: CapacitorConfig = {
  appId: 'com.nopsa.timemanagement',
  appName: 'Nopsa Pro',
  webDir: 'www',
  bundledWebRuntime: false,
  cordova: {
    preferences: {
      'android-minSdkVersion': '26',
      'android-targetSdkVersion': '30',
      'android-compileSdkVersion': '30',
      ScrollEnabled: 'false',
      BackupWebStorage: 'none',
      SplashMaintainAspectRatio: 'true',
      FadeSplashScreenDuration: '300',
      SplashShowOnlyFirstTime: 'true',
      SplashScreen: 'none',
      SplashScreenDelay: '0',
      ShowSplashScreen: 'false',
      AutoHideSplashScreen: 'false',
      AndroidXEnabled: 'true'
    }
  }
};

export default config;
