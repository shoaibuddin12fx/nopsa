import { formatDate } from '@angular/common';
import { AfterViewInit, Component, Injector, Input, OnInit } from '@angular/core';
import { Capacitor } from '@capacitor/core';
import { resolve } from 'path';
import { AlertsService } from 'src/app/services/basic/alerts.service';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { BasePage } from '../base-page/base-page';
import { EntitiesComponent } from './entities/entities.component';

@Component({
  selector: 'app-qr-in',
  templateUrl: './qr-in.page.html',
  styleUrls: ['./qr-in.page.scss'],
})
export class QrInPage extends BasePage implements OnInit, AfterViewInit {

  @Input() project_id;
  @Input() project;
  @Input() projectData;
  user;
  entities = [];
  entity;
  description;
  km_billable = 0;
  km_work = 0;
  daily_allowence = 0;
  lunch_allowence = 0;
  hideAllowence = true;
  isScanSuccess = false;
  public inTime;
  successMessageStart;
  successMessageEnd;
  formattedDate;
  showNotification = false;

  inTimeLoading = false;
  outTimeLoading = false;



  constructor(injector: Injector, public bobject: BobjectsService,) {
    super(injector);

  }

  ngOnInit() {

  }

  ngAfterViewInit(): void {

    this.initialize();
    this.getLitteras();
  }

  async initialize(): Promise<any> {

    return new Promise(async resolve => {

      this.user = await this.users.getUserData();
      // this.project_id = this.getQueryParams().project_id;
      if (this.project_id) {
        // this.projectData = JSON.parse(this.getQueryParams().project);
        // this.projectData = JSON.parse(this.getQueryParams().project);
        this.inTime = this.projectData.inTime;
        console.log("start:", this.inTime);
        console.log("Project Data:", this.projectData);
        console.log(this.inTime);
      } else {
        this.modals.dismiss();
      }


      resolve(true);
    })

  }

  async getLitteras(): Promise<any> {
    return new Promise(async resolve => {

      const bobj = await this.bobject.prepareObject();
      const obj = {
        "ownerId": parseInt(this.project_id)
      };

      const merged = { ...bobj, ...obj }
      console.log(merged)

      this.network.getAllLitteras(merged).then(async v => {
        console.log(v);
        this.entities = v['entities'];
        this.entity = this.entities.length > 0 ? this.entities[0] : null;
        if (this.inTime) {
          console.log("In time:", this.inTime)
          this.entity = await this.storage.getKey('qrin_entity');
          if (!this.entity) {
            this.entity = this.entities[0]
          }
        }
        resolve(true);
      })


    })
  }

  async selectEntity() {

    const _data = await this.modals.present(EntitiesComponent, { entities: this.entities })
    const data = _data.data;
    if (data['data'] != 'A') {
      this.entity = data;
    }

  }

  async qrcodeIn() {
    var self = this;
    this.inTimeLoading = true;
    const obj = await this.bobject.prepareObject();
    const data = await this.callNetworkResponse(obj, true);
    console.log('Getting data', { data });
    const lang = await this.bobject.translation.getLocale();
    
    const key = await this.storage.getKey('close_after_time_note');
    if (key == true) {
      console.log(key);
      self.showAlertBeforeCloseTimeNote().then(() => {
        setTimeout(() => {
          this.showNotification = !this.showNotification;
          this.closeApp()
          self.inTimeLoading = false;
        }, 3000)
      })
    }else{
      self.inTimeLoading = false;
    }
  }

  async qrcodeOut() {

    var self = this;
    this.outTimeLoading = true;

    const obj = await this.bobject.prepareObject();
    const data = await this.callNetworkResponse(obj, false);
    const key = await self.storage.getKey('close_after_time_note')
    console.log(key);
    if (key == true) {
      self.showAlertBeforeCloseTimeNote().then(() => {
        setTimeout(() => {
          self.showNotification = !self.showNotification;
          this.closeApp();
          self.outTimeLoading = false;
        }, 3000)
      })
    }else{
      self.outTimeLoading = false;
    }

  }

  callNetworkResponse(obj, flag): Promise<any> {

    return new Promise(resolve => {

      if (!this.entity) {
        resolve(null); return;
      }

      const _data = {
        "start": flag,
        "freeText": this.description,
        "vehicleCode": null,
        "kmBillable": this.km_billable,
        "kmWork": this.km_work,
        "amount": 0,
        "lunchAllowance": this.lunch_allowence,
        "dailyAllowance": this.daily_allowence,
        "litteraId": this.entity.id,
      }

      if (flag == true) {
        this.storage.setKey('qrin_entity', this.entity);
      }

      var merged = { ..._data, ...obj }
      console.log(merged)

      merged["latitude"] = this.projectData["latitude"];
      merged["longitude"] = this.projectData["longitude"];


      this.network.getItsQrcodeInOut(merged).then(res => {
        this.isScanSuccess = true;
        console.log(res);

        this.events.publish("projects:loadall");
        resolve(this.projectData);

      }, err => {
        console.log(err);
        this.isScanSuccess = false;
        resolve(null)

      })
    })

  }

  showAlertBeforeCloseTimeNote(): Promise<any> {
    return new Promise(async (resolve) => {

      const userObj = await this.bobject.prepareObject();
      console.log('Getting user object:', { userObj });
      //  console.log('ISO string: ',date); 
      this.formattedDate = this.utility.formatDate(userObj['timestamp']);
      this.successMessageStart = await this.translation.getTranslateKey('Success_Message_Start');

      //  console.log(formattedDate);
      this.successMessageEnd = await this.translation.getTranslateKey('Success_Message_End');
      //  const headerTitle = await this.translation.getTranslateKey('Info');
      this.showNotification = true;
      //  await this.utility.showAlert(`${successMessageStart} ${formattedDate} ${successMessageEnd}`,headerTitle);
      resolve(true);
    })
  }

  closeApp(){
    if(Capacitor.getPlatform() == 'android'){
      this.events.publish('main:closeapp');
    }
    else{
      this.modals.dismiss();
    }
  }

}
