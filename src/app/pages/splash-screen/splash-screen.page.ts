import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { MenuController } from '@ionic/angular';
import { Capacitor } from '@capacitor/core';

@Component({
  selector: 'app-splash-screen',
  templateUrl: './splash-screen.page.html',
  styleUrls: ['./splash-screen.page.scss'],
})
export class SplashScreenPage extends BasePage implements OnInit {

  splash = true;
  messages = 'loading';
  link = "https://nopsa.testi.loginets.com/kmls/jumping-pin.gif";
  constructor(injector: Injector, 
    private splashScreen: SplashScreen,
    private menu: MenuController,
    private androidPermissions: AndroidPermissions) { 
    super(injector);
    
    this.platform.ready().then(async () => {
      // this.splashScreen.hide();
      this.menu.swipeGesture(false);
      await this.initialize();
    });
    // this.events.subscribe('user:hidesplash',this.hideSplash.bind(this));
  }

  ionViewWillEnter() { 
    // setTimeout(() => this.splash = false, 4000);
    // this.initialize()
  }

  ngOnInit() {

  }



  async initialize(){

    function timeout(ms) {
      return new Promise(resolve => setTimeout(resolve, ms));
    }

    var permissions = Capacitor.getPlatform() != 'web' ? await this.checkPermissions() : await this.checkGeolocation();
    
    console.log("IS IT RETURNED HERE")
    var [args] = await Promise.all([
      permissions,
      timeout(500)
    ]);

    this.splash = false;
    console.log(this.splash);
    console.log("permissions", permissions);
    if(!permissions){
      this.messages = 'please_turn_on_location_permission'
      let msg = await this.translation.getTranslateKey('please_turn_on_location_permission')
      alert(msg);
      this.events.publish('exitapp');
    }
    else{
      this.events.publish('user:get');
    }

    
  }

  // hideSplash(){
  //   this.splash = false;
  // }
  checkGeolocation():Promise<any> {

    return new Promise(async resolve => {

      const coords = await this.utility.getCurrentLocationCoordinates();
      console.log(coords);
      resolve(coords)
    })
    
  }

  checkPermissions():Promise<any>{
    return new Promise((resolve) => {
      const permission = this.geolocation.checkGPSPermission()
      resolve(permission);
    })
  }

}
