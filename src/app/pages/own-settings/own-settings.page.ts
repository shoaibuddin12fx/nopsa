import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from '../base-page/base-page';
import { FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-own-settings',
  templateUrl: './own-settings.page.html',
  styleUrls: ['./own-settings.page.scss'],
})
export class OwnSettingsPage extends BasePage implements OnInit {

  languages: any[] = [];
  menus: any[] = [];
  close_after_time_note = true;



  aform: FormGroup;
  constructor(injector: Injector) { 
    super(injector)
    this.languages = this.translation.getTranslationsList();
    this.initializeSidemenuOptions()
  }

  ngOnInit() {
    
  }

  ionViewWillEnter(){
    this.storage.getKey('close_after_time_note').then( v => this.close_after_time_note = v )
  }

  async initializeSidemenuOptions(){
    this.menus = await this.sqlite.getUserSidemenu() as [];
  }

  onChangeLang(event: CustomEvent) {

    if (!event) return;

    const lang = event.detail.value;

    if (lang === 'ar') {
      document.dir = 'rtl';
    } else {
      document.dir = 'ltr';
    }

    this.translation.useTranslation(lang);
   
  }

  async toggleSidemenuItem(menu){
    // console.log(menu);
    await this.sqlite.setSidemenuItem(menu);
    this.events.publish('user:init_sidemenu')

  }

  toggleCloseAfterTimeNote($v){
    var self = this;
    self.storage.setKey('close_after_time_note', $v);
    
  }

  async showLogs(){
    this.nav.push('logs');
  }

}
