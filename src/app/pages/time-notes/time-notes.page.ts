import { Component, Injector, OnInit } from '@angular/core';
import { HeaderMenuComponent } from 'src/app/components/header-menu/header-menu.component';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { BasePage } from '../base-page/base-page';
import { QrInPage } from '../qr-in/qr-in.page';

@Component({
  selector: 'app-time-notes',
  templateUrl: './time-notes.page.html',
  styleUrls: ['./time-notes.page.scss'],
})
export class TimeNotesPage extends BasePage implements OnInit {

  projects: any[] = [];
  filterTerm;
  isloaded = false;

  loading = false;
  constructor(injector: Injector, public bobjectsService: BobjectsService) { 
    super(injector)
    // this.isloaded = false;
    this.initialize();
    
  }



  ngOnInit() {
    this.events.unsubscribe("projects:loadall");
    this.events.subscribe("projects:loadall", this.initialize.bind(this));
  }

  ionViewWillEnter() {

  }

  async initialize(){
    
    
    // if(this.loading == true){
    //   return;
    // }
    
    // this.loading = true;
    const bobj = await this.bobjectsService.prepareObject()
    const params = { type: "Active Projects" }
    const merged = {...bobj, ...params}
    // console.log(merged)
    // if(this.isloaded == true){
    //   return;
    // }

    this.network.getAllProjects(merged).then( v => {
      // console.log(v);
      this.isloaded = true;

      this.projects = v.projects.map(p => {
        var o = p;
        o['completeAddress'] = this.compileAddress(o.address);
        return o;
      });

      // console.log(this.projects);
      // this.loading = false;
    })
  }

  compileAddress(obj: any): string {

    function addSpace(str){
      return str ? str + ' ' : '';
    }

    var str = '';
    str += addSpace(obj.streetName1);
    str += addSpace(obj.streetName2);
    // str += addSpace(obj.entityName);
    str += addSpace(obj.city);
    str += addSpace(obj.country);
    str += addSpace(obj.postCode);
    obj.completeAddress = str;
    
    return str;

  }

  openProjectQrcode(item){
    // this.nav.push('qr_in', {project_id: item.id, project: JSON.stringify(item)})
    this.modals.present(QrInPage, {project_id: item.id, projectData:item} )
  }

  async moreMenu($event){
    
    const _data = await this.popover.present(HeaderMenuComponent, $event, {pid: null, flag: 'R' });
    const data = _data.data;
    // console.log(data);

    let flag = data.param;
    if(flag == "R"){
      this.initialize()
    }

  }



}
