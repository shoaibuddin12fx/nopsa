import { AppSettings } from './../../services/app-settings';
import { Component, Injector } from '@angular/core';
import { HomeService } from './../../services/home-service';
import { ModalController } from '@ionic/angular';
import { IntroPage } from '../intro-page/intro-page.page';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
  providers: [HomeService]
})
export class HomePage extends BasePage {

  item = {}
  
  constructor( injector: Injector,
    private homeService:HomeService, 
    public modalController: ModalController) { 
      super(injector);
      this.events.publish('user:init_sidemenu');



      this.item = this.homeService.getData();
      let showWizard = localStorage.getItem("SHOW_START_WIZARD");

      if (AppSettings.SHOW_START_WIZARD && !showWizard) {
        this.openModal()
      }
  }

  async openModal() {
    let modal = await this.modalController.create({component: IntroPage});
     return await modal.present();
  }
}
