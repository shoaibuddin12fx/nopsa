import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ReadqrcodePageRoutingModule } from './readqrcode-routing.module';

import { ReadqrcodePage } from './readqrcode.page';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NgxQRCodeModule,
    ReadqrcodePageRoutingModule
  ],
  declarations: [ReadqrcodePage]
})
export class ReadqrcodePageModule {}
