import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ReadqrcodePage } from './readqrcode.page';

describe('ReadqrcodePage', () => {
  let component: ReadqrcodePage;
  let fixture: ComponentFixture<ReadqrcodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReadqrcodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ReadqrcodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
