import { Component, OnInit } from '@angular/core';
import { BarcodeScannerOptions, BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { QRCodeService } from 'src/app/services/qrcode-service';
import { AlertController, LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LoadingService } from 'src/app/services/loading-service';
import { CommonService } from 'src/app/services/common.service';

@Component({
  selector: 'app-readqrcode',
  templateUrl: './readqrcode.page.html',
  styleUrls: ['./readqrcode.page.scss'],
  providers: [QRCodeService]
})
export class ReadqrcodePage implements OnInit {

  encodeData: any;
  scannedData: {};
  barcodeScannerOptions: BarcodeScannerOptions;
  title = 'app';
  elementType = 'url';
  value = 'FWE4090';
  createdCode: any;
  qrCodeMap = new Map();
  showMainBtn: boolean = true;
  scannedDataValue = new Set();
  tempArray: any = [];
  loading: any;
  noFound: boolean = false;

  constructor(private barcodeScanner: BarcodeScanner,
    private qrCodeService: QRCodeService,
    private alertCtrl: AlertController,
    private router: Router,
    private commonService: CommonService) {
    this.barcodeScannerOptions = {
      showTorchButton: true,
      showFlipCameraButton: true
    };
  }
  
  ionViewDidEnter(){
    console.log('inside load');
    this.scanCode();
  }

  ngOnInit(): void {
    console.log('inside init');
  }


  async scanCode() {
    this.barcodeScanner
      .scan()
      .then(async barcodeData => {
        this.scannedData = barcodeData.text;
        await this.validateQRCode(this.scannedData);
      })
      .catch(err => {
        console.log("Error", err);
      });
  }

  async validateQRCode(scannedDataTmp) {
    this.scannedData = scannedDataTmp;
    this.scannedDataValue.add(scannedDataTmp);
    this.showMainBtn = false;
    
    this.qrCodeService.validateQrCode(scannedDataTmp).subscribe(res => {
      if (res.responseData.type != 0) {
        this.qrCodeMap.set(res.responseData.id, res.responseData.type);
        this.bookNow();
      } else{
        this.noFound = true;
      }
    }, error => {
      //this.loading.dismissAll();
    });
  }

  ionViewDidLeave(){
    console.log('inside destroy');
    this.scannedData = "";
    this.scannedDataValue = new Set();
    this.showMainBtn = true;
  }

  createCode() {
    this.scannedData = this.value;
  }

  async bookNow() {
    for (let [key, value] of this.qrCodeMap) {
      if (value == 2) {
        this.tempArray.splice(0, this.tempArray.length)
        this.tempArray.push(key);
        
        this.commonService.addContainerData(this.tempArray);
        this.router.navigateByUrl("book-container");

        /* this.qrCodeService.insertContainer('Test', this.tempArray).subscribe(async res => {
          
          let alert = await this.alertCtrl.create({
            message: 'Booking done',
            buttons: [{
              text: 'Home',
              handler: data => {
                this.router.navigateByUrl("/home");
              }
            }, {
              text: 'Scan More',
              handler: data => {
                this.scannedData = "";
                this.scannedDataValue.clear();
                this.showMainBtn = true;
                this.scanCode();
              }
            }]
          });
          alert.present();
        }); */
      } else if (value == 1) {
        this.tempArray.splice(0, this.tempArray.length)
        this.tempArray.push[key];
        
        this.commonService.addPackageData(this.tempArray);
        this.commonService.addFromQRCode(true);
        this.router.navigateByUrl("book-package");

        /* this.qrCodeService.insertPackages('Test', this.tempArray).subscribe(async res => {

          let alert = await this.alertCtrl.create({
            message: 'Booking done',
            buttons: [{
              text: 'Home',
              handler: data => {
                this.router.navigateByUrl("/home");
              }
            }, {
              text: 'Scan More',
              handler: data => {
                this.scannedData = "";
                this.scannedDataValue.clear();
                this.showMainBtn = true;
                this.scanCode();
              }
            }]
          });
          alert.present();

        }); */
      } else if (value == 3) {
        this.tempArray.splice(0, this.tempArray.length)
        this.tempArray.push[key];

        this.commonService.addShipmentData(this.tempArray);
        this.router.navigateByUrl("book-store");
        
        /* this.qrCodeService.insertShipment('Test', this.tempArray).subscribe(async res => {

          let alert = await this.alertCtrl.create({
            message: 'Booking done',
            buttons: [{
              text: 'Home',
              handler: data => {
                this.router.navigateByUrl("/home");
              }
            }, {
              text: 'Scan More',
              handler: data => {
                this.scannedData = "";
                this.scannedDataValue.clear();
                this.showMainBtn = true;
                this.scanCode();
              }
            }]
          });
          alert.present();

        }); */
      }

    }
  }

}
