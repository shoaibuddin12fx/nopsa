import { NavService } from './../../services/nav.service';
import { Component, Injectable, Injector, OnInit } from '@angular/core';
import { NetworkService } from 'src/app/services/network.service';
import { UtilityService } from 'src/app/services/utility.service';
import { Location } from '@angular/common';
import { Platform, MenuController } from '@ionic/angular';
import { SqliteService } from 'src/app/services/sqlite.service';
import { EventsService } from 'src/app/services/events.service';
import { FormBuilder } from '@angular/forms'; 
// import { PopoversService } from 'src/app/services/basic/popovers.service';
import { UserService } from 'src/app/services/user.service';
import { ModalService } from 'src/app/services/basic/modal.service';
import { TranslationService } from 'src/app/services/translation-service.service';
import { StorageService } from 'src/app/services/storage.service';
import { ActivatedRoute, Router } from '@angular/router';
import { GeolocationsService } from 'src/app/services/geolocations.service';
import { PopoversService } from 'src/app/services/basic/popovers.service';

@Injectable()
export abstract class BasePage {

    public network: NetworkService;
    public utility: UtilityService;
    public nav: NavService;
    public activatedRoute: ActivatedRoute;
    public router: Router;
    public location: Location;
    public events: EventsService;
    public platform: Platform;
    public sqlite: SqliteService;
    public formBuilder: FormBuilder;
    public popover: PopoversService;
    public users: UserService;
    public modals: ModalService;
    public menuCtrl: MenuController;
    public translation: TranslationService;
    public storage: StorageService;
    public geolocation: GeolocationsService;
    

    constructor(injector: Injector) {
        this.platform = injector.get(Platform);
        this.sqlite = injector.get(SqliteService);
        this.users = injector.get(UserService);
        this.network = injector.get(NetworkService);
        this.utility = injector.get(UtilityService);
        this.location = injector.get(Location);
        this.events = injector.get(EventsService);
        this.nav = injector.get(NavService);
        this.formBuilder = injector.get(FormBuilder);
        this.popover = injector.get(PopoversService);
        this.modals = injector.get(ModalService);
        this.menuCtrl = injector.get(MenuController)
        this.translation = injector.get(TranslationService)
        this.storage = injector.get(StorageService)
        this.activatedRoute = injector.get(ActivatedRoute);
        this.router = injector.get(Router);
        this.geolocation = injector.get(GeolocationsService)

    }

    getRouterParams(key) {
        return this.activatedRoute.snapshot.params[key];
    }

    getParams() {
        return this.activatedRoute.snapshot.params;
    }

    getQueryParams() {
        return this.activatedRoute.snapshot.queryParams;
    }

    
}