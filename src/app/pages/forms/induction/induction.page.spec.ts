import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { InductionPage } from './induction.page';

describe('InductionPage', () => {
  let component: InductionPage;
  let fixture: ComponentFixture<InductionPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InductionPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(InductionPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
