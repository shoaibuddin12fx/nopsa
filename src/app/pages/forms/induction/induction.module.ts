import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InductionPageRoutingModule } from './induction-routing.module';

import { InductionPage } from './induction.page';
import { IonicSelectableModule } from 'ionic-selectable';
import { TranslateModule } from '@ngx-translate/core';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { FormsPage } from '../forms.page';
import { FirstscreenComponent } from '../components/firstscreen/firstscreen.component';
import { SecondscreenComponent } from '../components/secondscreen/secondscreen.component';
import { ThirdscreenComponent } from '../components/thirdscreen/thirdscreen.component';
import { FourthscreenComponent } from '../components/fourthscreen/fourthscreen.component';
import { IntroductiongroupdetailComponent } from '../components/introductiongroupdetail/introductiongroupdetail.component';
import { IntroductiongroupfilesComponent } from '../components/introductiongroupfiles/introductiongroupfiles.component';
import { FileListComponent } from '../../site-diary/Components/file-list/file-list.component';
import { WizardComponent } from '../components/wizard/wizard.component';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    IonicSelectableModule,
    InductionPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    Ng2SearchPipeModule
  ],
  declarations: [InductionPage,FormsPage, FirstscreenComponent, 
    SecondscreenComponent, 
    ThirdscreenComponent, 
    FourthscreenComponent, 
    IntroductiongroupdetailComponent,
    IntroductiongroupfilesComponent,
    FileListComponent,
    WizardComponent
  ]
})
export class InductionPageModule {}
