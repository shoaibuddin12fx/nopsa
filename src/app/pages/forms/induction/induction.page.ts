import { Component, Injector, OnInit } from '@angular/core';
import { IonicSelectableComponent } from 'ionic-selectable';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { LoadingService } from 'src/app/services/loading-service';
import { ProjectsService } from 'src/app/services/projects.service';
import { UserlistService } from 'src/app/services/userlist.service';
import { BasePage } from '../../base-page/base-page';
import { IntroductiongroupdetailComponent } from '../components/introductiongroupdetail/introductiongroupdetail.component';
import { FormsPage } from '../forms.page';

@Component({
  selector: 'app-induction',
  templateUrl: './induction.page.html',
  styleUrls: ['./induction.page.scss'],
})
export class InductionPage extends BasePage implements OnInit {

  public id: number;
  public name: string;
  ports: any[];
  project: any;
  plist = [];
  ulist = [];
  introductionList = [];
  diaryList = [];
  projectId;
  term;
  user:any;
  userId:any;
  cancelText:string;
  searchText:string;
  noProjectsFound:string;
  noUsersFound:string;
  bObj;
  constructor(injector: Injector, public bobjectsService: BobjectsService, public projectService:ProjectsService, public userListService:UserlistService,public loadingService:LoadingService) { 
    super(injector)
    this.initialize();
    this.events.subscribe("Forms Component: Refresh Inducted User List",async ()=>{
      this.introductionList = await this.getIntroductions(this.userId)
    })
  }

  ngOnInit() {
  }


  async initialize() {
    this.cancelText = await this.translation.getTranslateKey("cancelText");
    this.searchText = await this.translation.getTranslateKey("search");
    this.noProjectsFound = await this.translation.getTranslateKey("error_no_projects");
    this.noUsersFound = await this.translation.getTranslateKey("error_no_users");
    this.bObj = await this.bobjectsService.prepareObject();
    this.plist = await this.projectService.getProjects();
    this.project = this.plist[0];

    if(this.projectId){
      this.ulist = await this.userListService.getUsersList({...this.bObj,ownerId:this.projectId})
      this.user = this.ulist[0];
      this.userId = this.ulist[0].id
      this.introductionList = await this.getIntroductions(this.userId)
      console.log(this.introductionList);
    }
    else{
      this.ulist = await this.userListService.getUsersList({...this.bObj,ownerId:this.project.id})
      this.user = this.ulist[0];
      this.userId = this.ulist[0].id
      this.introductionList = await this.getIntroductions(this.userId)
    }
  }


  // getProjectDiaryLogs(id):Promise<any[]>{
  //   return new Promise(async (resolve,reject) =>{
  //     const bObj = await this.bobjectsService.prepareObject();
  //     const idObj = {ownerId:id}
  //     const merged = {...bObj,...idObj};
  //     this.network.getLogs(merged).then(res=>{
  //       resolve(res['entities']);
  //       reject(()=>this.utility.presentFailureToast("Failure"))
  //     })
  //   })
  // }

  async openInductionFormPage(){
    const res = await this.modals.present(FormsPage,{project:this.project});
  }

  async projectChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    this.projectId = event.value.id;
    const bObj = await this.bobjectsService.prepareObject();
    this.ulist = await this.userListService.getUsersList({...bObj,ownerId:this.projectId})
    this.user = this.ulist[0];
    console.log(this.ulist);
  }

  async userChange(event:{
    component:IonicSelectableComponent,
    value: any
  }){
    this.userId = event.value.id;
    this.introductionList = await this.getIntroductions(this.userId);
    console.log(this.user)
    console.log(this.introductionList);
  }

  getIntroductions(userId):Promise<any[]>{
    return new Promise(async(resolve,reject)=>{
    const bObj = await this.bobjectsService.prepareObject();
    const merged = {...bObj,userId:userId}
    this.network.getCreatedIntroductionGroups(merged).then(data=>{
      resolve(data["introductions"]);
    }).catch(err=>{
      reject(err);
    })
  })
  }

  async goToIntroductionDetail(introductionGroupDetail){
    const res = await this.modals.present(IntroductiongroupdetailComponent,{detail:introductionGroupDetail})

    const data = res.data;
  }

  back(){
    this.nav.pop();
  }
}
