import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FormsPage } from './forms.page';
import { TranslateModule } from '@ngx-translate/core';
import { InductionPageRoutingModule } from './induction/induction-routing.module';
import { WizardComponent } from './components/wizard/wizard.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InductionPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    WizardComponent
  ],
  declarations: [FormsPage]
})
export class FormsPageModule { }
