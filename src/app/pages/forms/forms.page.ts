import { Component, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { ProjectService } from 'src/app/services/project.service';
import { ProjectsService } from 'src/app/services/projects.service';
import { BasePage } from '../base-page/base-page';
import { DiaryDetailOptionsComponent } from '../site-diary/Components/diary-detail-options/diary-detail-options.component';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { PictureUploadComponent } from '../site-diary/Components/picture-upload/picture-upload.component';
import { UserlistService } from 'src/app/services/userlist.service';
import { GroupsService } from 'src/app/services/groups.service';
@Component({
  selector: 'app-forms',
  templateUrl: './forms.page.html',
  styleUrls: ['./forms.page.scss'],
})
export class FormsPage extends BasePage implements OnInit {


  @ViewChild('slides', { static: true }) slides: IonSlides;
  @Input() _project;
  get project():any{
    return this._project;
  }

  @Input() set project(value : any){
    this._project = value;
    
    this.getIntroGroups(this.project.id).then( v => {
      this.groups = v;
    });
  }

  stepCompleted = true;
  firstStepCompleted;
  secondStepCompleted;
  thirdStepCompleted;
  fourthStepCompleted;
  id;
  // @Input() project;
  image: string;
  mimeType: string;
  ids = [];
  userInfo = [];
  submitted;
  step;
  groups = [];
  usersList = [];
  attributeInfo = [];
  stepNo = 0;
  userAttributes = [];
  groupAttributes = [];
  filteredUsers = [];
  filteredUserAttributes = [];
  group;
  slideNum = 0;
  groupCreationData;
  disableThirdStep: boolean = false;
  constructor(injector: Injector, public cameraService: Camera, public bobjectsService: BobjectsService, public userListService: UserlistService, public groupsService: GroupsService) {
    super(injector);

    this.events.subscribe("user is Checked", (data) => {
      this.filteredUsers = this.utility.addValuesInArrayBasedOnSelection(data.user, true, this.userInfo);
      console.log(this.filteredUsers);
    })

    this.events.subscribe("attribute checked", (data) => {
      console.log(data);
      this.attributeInfo = this.utility.addValuesInArrayBasedOnSelection(data.attribute, true, this.attributeInfo);
    })

    this.events.subscribe("secondscreen:group Value Changed", (data) => {
      console.log(data);
      this.id = data.id;
      this.setGroupAttributes(data.id);
    })

    this.events.subscribe("FourthScreenComponent:Valid", (data) => {
      this.groupCreationData = data;
      console.log(this.groupCreationData);

      this.createGroup(this.groupCreationData).then(() => {
        this.events.publish("Forms Component: Refresh Inducted User List");
        this.modals.dismiss();
      })
      console.log(data);
    })



  }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    this.getCurrentUsers().then(() => {
      this.getGroupsForSelectedProjects();
    })
  }

  async goToNext() {
    let activeIndex = await this.slides.getActiveIndex();
    if (activeIndex == 0) {
      if (this.filteredUsers.length > 0) {
        this.firstStepCompleted = true;
        this.moveSlide(activeIndex + 1);
      }
      else {
        const noUsersSelected = await this.translation.getTranslateKey("choose_user");
        this.utility.presentFailureToast(noUsersSelected);
      }
    }
    if (activeIndex == 1) {
      if (this.attributeInfo.length > 0 && this.filteredUsers.length == 1) {
        this.secondStepCompleted = true;
        this.setUserAttributes(this.usersList[0].id);
        this.moveSlide(activeIndex + 1);
      }
      else if (this.attributeInfo.length > 0 && this.filteredUsers.length > 1) {
        this.secondStepCompleted = true;
        this.thirdStepCompleted = true;
        this.disableThirdStep = true;
        console.log(this.disableThirdStep);
        this.setUserAttributes(this.usersList[0].id);
        this.moveSlide(activeIndex + 2);
      }
      else {
        const noGroupAttribute = await this.translation.getTranslateKey("choose_group_attribute");
        this.utility.presentFailureToast(noGroupAttribute);
      }
    }

    if (activeIndex == 2) {
      if (this.filteredUserAttributes.length > 0) {
        this.moveSlide(activeIndex + 1);
        this.thirdStepCompleted = true;
      }
      else {
        const noUserAttribute = await this.translation.getTranslateKey("choose_user_attribute");
        this.utility.presentFailureToast(noUserAttribute);
      }
    }

    if (activeIndex == 3) {
      this.validate();
    }
  }

  // goToNext($event){
  //   if($event.step == 0 && $event.completed == true){
  //     this.firstStepCompleted = $event.completed;
  //     this.id = $event.groupId;
  //     this.stepOneInfo = $event;
  //     this.moveSlide($event.step+1);
  //   }
  //   else if($event.step == 1 && $event.completed == true && $event.users.length == 1){
  //     this.secondStepCompleted = $event.completed;
  //     console.log($event);
  //     this.stepTwoInfo = $event;
  //     console.log(this.stepTwoInfo);
  //     this.moveSlide($event.step+1);
  //   }
  //   else if($event.step == 2 && $event.completed == true){
  //     this.thirdStepCompleted = $event.completed;
  //     console.log($event);
  //     this.stepThreeInfo = $event;
  //     console.log(this.stepThreeInfo);
  //     this.moveSlide($event.step+1);
  //   }
  // }

  getIntroGroups(id):Promise<any[]>{
    return new Promise(async (resolve,reject)=>{
      const bObj = await this.bobjectsService.prepareObject();
      const merged = {...bObj,ownerId:id}
      this.network.getIntroductionGroups(merged).then((data)=>{
        resolve(data["entities"]);
      }).catch(err=>reject(err))
    })
  }

  async getCurrentUsers() {
    const bObj = await this.bobjectsService.prepareObject();
    const merged = { ...bObj, ownerId: this.project.id }
    this.usersList = await this.userListService.getUsersList(merged);
  }

  async getGroupsForSelectedProjects() {
    const bObj = await this.bobjectsService.prepareObject();
    const merged = { ...bObj, ownerId: this.project.id }
    this.groups = await this.groupsService.getIntroGroups(merged);
  }

  async setGroupAttributes(id) {
    const bObj = await this.bobjectsService.prepareObject();
    const merged = { ...bObj, ownerId: id }
    console.log(merged);
    this.groupAttributes = await this.groupsService.getSelectedGroupAttributes(merged);
  }

  async setUserAttributes(id) {
    const bObj = await this.bobjectsService.prepareObject();
    const merged = { ...bObj, id: id };
    this.userAttributes = await this.userListService.getUserAttributes(merged);
    this.events.publish("Send user attributes to third screen", { userAttributes: this.userAttributes });
    console.log(this.userAttributes);
  }

  validate() {
    console.log("validating...")
    this.events.publish("form:Create Introduction Group");
  }

  async createGroup(groupCreationData) {
    const bObj = await this.bobjectsService.prepareObject();

    const selectedUserIds = this.filteredUsers.map(val => {
      return val.id;
    })

    const selectedIntroGroupAttributes = this.attributeInfo.map(val => {

      let filteredObj = {
        "value": val["checked"] ? "yes" : "no",
        "introGroupAttributeId": val["id"]
      }
      return filteredObj
    })


    const merged = {
      ...bObj,
      entity: {
        "introGroupId": this.id,
        "selectedUserIds": selectedUserIds,
        "dateSigned": new Date(groupCreationData.creationData.dateSigned).getTime(),
        "introducter": groupCreationData.creationData.introducer,
        "introAttributes": selectedIntroGroupAttributes,
        "userAttributes": this.filteredUserAttributes,
        "signatures": [{
          "base64": groupCreationData.imageOne
        },
        {
          "base64": groupCreationData.imageTwo
        }]
      }
    }
    if (this.filteredUsers.length == 1) {
      this.groupsService.createIntroductionForSingleUser(merged)
    }
    else {
      this.groupsService.createIntroductionForGroup(merged)
    }
  }

  moveSlide(num) {
    this.slideNum = num;
    this.slides.lockSwipes(false);
    this.slides.slideTo(num, 500);
    this.slides.lockSwipes(true);
  }

  back() {
    this.modals.dismiss();
  }

  onReceive($event) {
    console.log($event);
    if ($event.checked == true) {
      this.filteredUserAttributes.push($event);
    }
    else {
      const index = this.filteredUserAttributes.indexOf($event.id);
      this.filteredUserAttributes.splice(index, 1);
    }
  }

  async goStep($event) {
    const num = await this.slides.getActiveIndex();
    if ($event < num) {
      this.slideNum = $event;
      this.moveSlide($event);
    }
  }
}
