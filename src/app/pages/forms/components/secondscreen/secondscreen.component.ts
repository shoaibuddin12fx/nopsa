import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { LoadingService } from 'src/app/services/loading-service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-secondscreen',
  templateUrl: './secondscreen.component.html',
  styleUrls: ['./secondscreen.component.scss'],
})
export class SecondscreenComponent extends BasePage implements OnInit {

  _groups;
  _attribute;
  _project;
  _currentUsers;
  defaultValue;
  hidden = true;
  @Output() secondStep: EventEmitter<any> = new EventEmitter();

  get groups(): any {
    return this._groups;
  }

  get groupAttributes(): any{
    return this._attribute
  }

  get currentProject(): any{
    return this._project;
  }

  get currentUsers(): any{
    return this._currentUsers;
  }
  @Input() set groups(value: any) {
    this._groups = value;
    this.setDefaultValue(this._groups[0].id)
  }

  @Input() set groupAttributes(value:any){
    this._attribute = value;
    console.log(this._attribute);
  }

  @Input() set currentProject(value: any){
    this._project = value;
  }

  @Input() set currentUsers(value: any){
    this._currentUsers = value;
  }

  constructor(injector: Injector, public userService:UserService) {
    super(injector);
  }

  ngOnInit() {
    // this.getCurrentUser();
  }

  // async getCurrentUser(){
  //   this.currentUser = await this.userService.getUserData();
  //   console.log(this.currentUser);
  // }

  setDefaultValue(value){
    this.defaultValue = value;
  }
  getCheckedAttribute(attribute) {
    this.events.publish("attribute checked",{attribute:attribute});
  }

  getSelectedGroup(event){
    this.events.publish("secondscreen:group Value Changed",{id:event.target.value});
  }
}
