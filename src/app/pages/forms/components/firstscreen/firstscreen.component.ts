import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { LoadingService } from 'src/app/services/loading-service';
import { ProjectsService } from 'src/app/services/projects.service';

@Component({
  selector: 'app-firstscreen',
  templateUrl: './firstscreen.component.html',
  styleUrls: ['./firstscreen.component.scss'],
})
export class FirstscreenComponent extends BasePage implements OnInit {

  _usersList;
  _group;
  defaultGroupName;
  term;
  noUsersFound:string;
  search:string;
  get userList():any{
    return this._usersList;
  } 

  get groups():any{
    return this._group;
  }
  
  @Input() set user(value:any){
    this._usersList = value;
  }
  
  constructor(injector:Injector,public projectsService:ProjectsService,public bObject:BobjectsService,public loadingService:LoadingService) { 
    super(injector)
  }
  ngOnInit() {
    this.initialize();
  }

  async initialize(){
    this.noUsersFound = await this.translation.getTranslateKey("error_no_users");
    this.search = await this.translation.getTranslateKey("search");
  }


  getCheckeduser(user){
    this.events.publish("user is Checked",{user:user});
  }

  validate(){

  }
}
