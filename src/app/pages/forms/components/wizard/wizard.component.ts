import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss'],
})
export class WizardComponent implements OnInit {

  _disableThirdStep;
  @Input() step: any;
  @Output() goStep: EventEmitter<any> = new EventEmitter<any>();
  @Input() set disableThirdStep(value:boolean){
    this._disableThirdStep = value;
    console.log(this._disableThirdStep);
  };

  get disableThirdStep(){
    return this._disableThirdStep;
  }
  constructor() { }

  ngOnInit() {
    console.log(this.step);
  }

}
