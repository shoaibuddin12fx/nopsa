import { AfterViewInit, Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { DiaryDetailOptionsComponent } from 'src/app/pages/site-diary/Components/diary-detail-options/diary-detail-options.component';
import { PictureUploadComponent } from 'src/app/pages/site-diary/Components/picture-upload/picture-upload.component';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { ProviderMeta } from '@angular/compiler';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';
import SignaturePad from 'signature_pad';
@Component({
  selector: 'app-fourthscreen',
  templateUrl: './fourthscreen.component.html',
  styleUrls: ['./fourthscreen.component.scss'],
})
export class FourthscreenComponent extends BasePage implements OnInit, AfterViewInit {
  group: FormGroup;
  defaultDate: any = new Date().toISOString();
  image;
  mimeType;
  @ViewChild('foremanSign', { static: true }) signatureOnePadElement;
  @ViewChild('workerSign', { static: true }) signatureTwoPadElement;
  signatureOnePad: any;
  signatureTwoPad: any;
  canvasWidth: number;
  canvasHeight: number;
  img1;
  img2
  submitted = false;

  constructor(injector: Injector, private elementRef: ElementRef, public cameraService: Camera, public bobjectsService: BobjectsService) {
    super(injector);
    this.events.subscribe("form:Create Introduction Group",this.validated.bind(this));
  }

  ngOnInit() {
    this.intialize();
    const canvas1: any = this.elementRef.nativeElement.querySelector('foremanSign');
    const canvas2: any = this.elementRef.nativeElement.querySelector('workerSign');
    canvas1.width = window.innerWidth - 200;
    canvas1.height = window.innerHeight - 140;
    console.log(canvas1.width)
    console.log(canvas1.height)
    canvas2.width = window.innerWidth -200;
    canvas2.height = window.innerHeight - 140;
    if (this.signatureOnePad && this.signatureTwoPad) {
      this.signatureOnePad.clear(); 
      this.signatureTwoPad.clear(); 
    }
  }

  public ngAfterViewInit() {
    this.signatureOnePad = new SignaturePad(this.signatureOnePadElement.nativeElement);
    this.signatureOnePad.clear();
    this.signatureOnePad.penColor = 'rgb(56,128,255)';
    this.signatureTwoPad = new SignaturePad(this.signatureTwoPadElement.nativeElement);
    this.signatureTwoPad.clear();
    this.signatureTwoPad.penColor = 'rgb(56,128,255)';
  }

  clearPadOne(){
    this.signatureOnePad.clear();
  }

  clearPadTwo(){
    this.signatureTwoPad.clear();
  }

  save() {
    this.img1 = this.signatureOnePad.toDataURL();
    this.img2 = this.signatureTwoPad.toDataURL();
  }

  createForm() {
    this.group = this.formBuilder.group({
      dateSigned: [null, [Validators.required]],
      introducer: [null, [Validators.required]],
    })
  }

  validated(){
    this.save();
    this.submitted = true;
    if(this.group.valid && this.img1 != null && this.img2 != null){
      console.log("data here")
      this.events.publish("FourthScreenComponent:Valid",{creationData:this.group.value,imageOne:this.img1,imageTwo:this.img2})
    }
  }

  async intialize() {
    this.createForm();
    const currentUser = await this.users.getUserData();
    console.log(currentUser);
    this.group.controls["introducer"].setValue(`${currentUser.firstname} ${currentUser.lastname}`)
  }

  // async postIntroductionGroup() {
  //   const bObj = await this.bobjectsService.prepareObject();
  //   const selectedUserIds = this.thirdStepInfo.info["info"].users.map(val => {
  //     return val.id;
  //   })
  //   const selectedIntroGroupAttributes = this.thirdStepInfo["info"].selectedAttributes.map(val => {

  //     let filteredObj = {
  //       "value": val["checked"] ? "yes" : "no",
  //       "introGroupAttributeId": val["id"]
  //     }
  //     return filteredObj
  //   })

  //   const merged = {
  //     ...bObj,
  //     entity: {
  //       "introGroupId": this.thirdStepInfo.info["info"].groupId,
  //       "selectedUserIds": selectedUserIds,
  //       "dateSigned": new Date(this.group.controls["dateSigned"].value).getTime(),
  //       "introducter": this.group.controls["introducer"].value,
  //       "introAttributes": selectedIntroGroupAttributes,
  //       "userAttributes": this.thirdStepInfo["userAttributes"],
  //       "signatures": [
  //         {
  //           "base64": this.img1
  //         },
  //         {
  //           "base64": this.img2
  //         },
  //       ]
  //     }
  //   }

  //   if (selectedUserIds.length == 1) {
  //     this.createIntroductionForSingleUser(merged);
  //   }
  //   else {
  //     this.createIntroductionForGroup(merged);
  //   }
  // }

  // async onSubmit() {
  //   this.save();
  //   let attributeVals = await this.postIntroductionGroup();
  //   console.log(attributeVals);
  // }
}
