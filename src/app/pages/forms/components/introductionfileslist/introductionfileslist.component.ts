import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { EventsService } from 'src/app/services/events.service';
import { LoadingService } from 'src/app/services/loading-service';
import { NetworkService } from 'src/app/services/network.service';

@Component({
  selector: 'app-introductionfileslist',
  templateUrl: './introductionfileslist.component.html',
  styleUrls: ['./introductionfileslist.component.scss'],
})
export class IntroductionfileslistComponent extends BasePage implements OnInit {

  _item: any;
  _urls: any;
  _userDetails: any;
  params: any;
  path: any;
  ids = [];
  checkbox_hidden: boolean = true;
  get item(): any {
    return this._item;
  }

  get urls(): any {
    return this._urls;
  }

  get userDetails(): any {
    return this._userDetails;
  }
  @Input() set item(value: any) {
    this._item = value;
    // call image details api here in this component 
    // and get the image and show inside any image tag in html for preview
  }

  @Input() set urls(value: any) {
    this._urls = value;
  }

  @Input() set userDetails(value: any) {
    this._userDetails = value;
  }

  
  @Output('openFile') openFile: EventEmitter<any> = new EventEmitter<any>();

  constructor(injector: Injector, public network: NetworkService, public bobjectsService: BobjectsService, public events: EventsService,public loaderService:LoadingService) {
   
    super(injector);
    this.events.subscribe('checkbox_hidden', () => {
      this.checkbox_hidden = !this.checkbox_hidden;
    })
    this.events.subscribe("delete", () => {
        this.deleteFiles();
    })
    this.initialize();
  }

  ngOnInit() {
  }


  async initialize() {
    console.log(this.item);
    this.path = await this.retreieveImageURL(this.item.id);
  }

  sendPictureClickEvent(path) {
    console.log('event published')
    this.events.publish('Picture Clicked', path);
  }

  emitOpenFile(path) {
    console.log(path);
    if (this.checkbox_hidden == true) {
      this.openFile.emit(path)
    }
  }

  retreieveImageURL(entity): Promise<any> {
    return new Promise(async (resolve, reject) => {
      var obj = await this.bobjectsService.prepareObject();
      console.log(entity)
      this.params = { ...obj, id: entity.id };
      this.network.getUploadedImage(this.params).then(data => {
        resolve(data.responseData);
      }).catch(err =>{
        reject(err);
      })
    })
  }

  deleteFiles(){
      this.events.publish("ondelete",{ids:this.ids})
  }

  addId($event) {
    if (this.item.checked == true) {
      this.ids.push(this.item.id);
    }
    else {
      const index = this.ids.indexOf(this.item.id);
      this.ids.splice(index, 1);
    }
  }

  doRefreshwithRefresher($event) {
    this.path = null;
    if (this.path == null) {
      this.retreieveImageURL(this.item)
        .then((data) => {
          this.path = data;
          $event.target.complete()
        }).then(() => {
          this.events.publish('Refreshed');
        })
    }
  }

  Refresh() {
    this.path = null;
    if (this.path == null) {
      this.retreieveImageURL(this.item)
        .then((data) => {
          this.path = data;
        }).then(() => {
          this.events.publish('Refreshed');
        })
    }
  }
}

