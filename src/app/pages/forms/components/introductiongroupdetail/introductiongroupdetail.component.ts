import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { DiaryDetailOptionsComponent } from 'src/app/pages/site-diary/Components/diary-detail-options/diary-detail-options.component';
import { IntroductiongroupfilesComponent } from '../introductiongroupfiles/introductiongroupfiles.component';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { BobjectsService } from 'src/app/services/bobjects.service';
@Component({
  selector: 'app-introductiongroupdetail',
  templateUrl: './introductiongroupdetail.component.html',
  styleUrls: ['./introductiongroupdetail.component.scss'],
})
export class IntroductiongroupdetailComponent extends BasePage implements OnInit {

  detail;
  image: string;
  mimeType: string;
  constructor(injector:Injector,public cameraService:Camera, public bobjectService:BobjectsService) { 
    super(injector);
  }

  ngOnInit() {
    console.log(this.detail)
  }
  
  dismiss(){
    this.modals.dismiss();
  }


  async activatePopover(ev){
    const _data = await this.popover.present(DiaryDetailOptionsComponent,ev,{flag:'IND'});
  
    const data = _data.data;
  
    if(data){
      let flag = data.param;
  
      if(flag == "GP"){
        this.takePhotoPage();
      }
      if(flag == "GT"){
        this.openCamera();
      }
    }
  }

  async takePhotoPage(){
    const res = await this.modals.present(IntroductiongroupfilesComponent,{detail:this.detail})
  
    const data = res.data;
  
    console.log(data);
  }

  openCamera(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.cameraService.DestinationType.DATA_URL,
      encodingType: this.cameraService.EncodingType.JPEG,
      mediaType: this.cameraService.MediaType.PICTURE,
      correctOrientation: true
    }

    this.cameraService.getPicture(options).then(async (imageData) => {
      this.image = 'data:image/jpeg;base64,'+imageData;
      this.mimeType = 'image/jpeg';
      this.uploadFile();
    })
  }


  async uploadFile() {

    var obj1 = await this.bobjectService.prepareObject();

    let obj2 = {
      'type': "INTRODUCTION",
      'id':this.detail.id,
      'base64': this.image
    }

     var obj = { ...obj1, ...obj2 }

    console.log(obj);
    // console.log(formdata);

      var uploaded = await this.network.uploadImage(obj);
      console.log(uploaded);
  }

}
