import { Component, EventEmitter, Injector, Input, OnInit, Output } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { LoadingService } from 'src/app/services/loading-service';

@Component({
  selector: 'app-thirdscreen',
  templateUrl: './thirdscreen.component.html',
  styleUrls: ['./thirdscreen.component.scss'],
})
export class ThirdscreenComponent extends BasePage implements OnInit {

  userAttributes = [];

  @Output('sendValuesToMain') sendValuesToMain: EventEmitter<any> = new EventEmitter<any>();
  constructor(injector: Injector, public bObjectService: BobjectsService, public loaderService: LoadingService) {
    super(injector);
    this.events.subscribe("Send user attributes to third screen",(attributes)=>{
      console.log("Being recieved from second screen")
      this.userAttributes = attributes.userAttributes;
    })
  }

  ngOnInit() {
    // this.initialize();
  }


  async initialize() {
  }

  

  

  getCheckedUserAttributes(attribute) {
    console.log("wprkign");
    console.log(attribute);
    this.sendValuesToMain.emit(attribute);
  }
}
