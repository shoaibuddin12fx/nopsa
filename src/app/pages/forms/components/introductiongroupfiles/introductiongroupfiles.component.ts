import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { Camera,CameraOptions } from '@ionic-native/camera/ngx';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { LoadingService } from 'src/app/services/loading-service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
@Component({
  selector: 'app-introductiongroupfiles',
  templateUrl: './introductiongroupfiles.component.html',
  styleUrls: ['./introductiongroupfiles.component.scss'],
})
export class IntroductiongroupfilesComponent extends BasePage implements OnInit {

  detail;
  image: string;
  mimeType: string;
  @ViewChild('fileupload', { static: true }) fileupload: ElementRef<HTMLElement>;
  inputFile: File;
  imageList;
  bObj;
  checkbox_show;
  ids = [];
  uploadFileResponse;
  constructor(injector:Injector,public bobjectsService: BobjectsService,public cameraService:Camera,public loadingService:LoadingService, public photoViewer:PhotoViewer) { 
    super(injector);
      this.events.subscribe("addIdcheckedOrNot",(data)=>{
        this.addIdToIdsArray(data.item)
      })
  }

  ngOnInit() {
    this.initialize();
  }


  async initialize(){
    this.bObj = await this.bobjectsService.prepareObject();
    this.imageList = await this.retrieveImageList();
  }

  openFile(){
    let el: HTMLElement = this.fileupload.nativeElement;
    el.click();
  }


  openCamera(){
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.cameraService.DestinationType.DATA_URL,
      encodingType: this.cameraService.EncodingType.JPEG,
      mediaType: this.cameraService.MediaType.PICTURE,
      correctOrientation: true
    }

    this.cameraService.getPicture(options).then(async (imageData) => {
      const image = 'data:image/jpeg;base64,'+imageData;
      this.mimeType = 'image/jpeg';
      this.uploadFileResponse = await this.uploadFile(image);
      this.initialize();
    })
  }


  async uploadFile(image):Promise<any> {
    return new Promise(async (resolve,reject)=>{
      
    var obj1 = await this.bobjectsService.prepareObject();

    let obj2 = {
      'type': "INTRODUCTION",
      'id':this.detail.id,
      'base64': image
    }

     var obj = { ...obj1, ...obj2 }

    console.log(obj);

      this.network.uploadImage(obj).then(data =>{
        resolve(data);
      }).catch(err =>{
        reject(err);
      })
      
    })
  }


  onSelectFile(event) {

    console.log(event)
    let file = <File>event.target.files[0];
    if (file) {

      this.inputFile = file;
      this.mimeType = file.type;
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = async (e: any) => {
        // this.image = e.target.result;
        console.log(e.target.result);
        this.uploadFileResponse = await this.uploadFile(e.target.result);
        this.initialize();
        console.log(this.uploadFileResponse);
      }
      console.log(file);
    }
  }


  retrieveImageList():Promise<any[]>{
    return new Promise(async (resolve,reject)=>{
      const bObj = await this.bobjectsService.prepareObject();
      const intro = {
        'type': "INTRODUCTION",
        'id': this.detail.id
      } 

      const merged = {...bObj,...intro};
      this.network.getFileList(merged).then((data) => {
        resolve(data["entities"]);
      }).catch(err => {
        reject(err)
      })
    })
  }

  dismiss(){
    this.modals.dismiss();
  }

  viewFile($event) {
    if ($event.includes('jpeg') || $event.includes('png')) {
      this.photoViewer.show($event);
    }
    else {
      window.location.href = $event;
    }
  }

  hideCheckbox() {
    this.events.publish('checkbox_hidden');
  }


  deleteIntroductionFiles(ids):Promise<any>{
    return new Promise(async (resolve,reject)=>{
      var obj1 = await this.bobjectsService.prepareObject();
      const merged = { ...obj1, ids: ids }
      this.network.removeImage(merged).then(data =>{
        resolve(data)
      })
      .then(()=>{
        this.initialize();
      })
      .catch(err =>{
        reject(err);
      })
    })
  }

  addIdToIdsArray(item){
    if (item.checked == true) {
      this.ids.push(item.id);
    }
    else {
      const index = this.ids.indexOf(item.id);
      this.ids.splice(index, 1);
    }
  }

  doRefresh($event){
    this.initialize();
    this.hideCheckbox();
    this.checkbox_show = false;
    $event.target.complete();
  }
}
