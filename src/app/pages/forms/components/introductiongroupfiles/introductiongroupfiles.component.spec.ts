import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { IntroductiongroupfilesComponent } from './introductiongroupfiles.component';

describe('IntroductiongroupfilesComponent', () => {
  let component: IntroductiongroupfilesComponent;
  let fixture: ComponentFixture<IntroductiongroupfilesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IntroductiongroupfilesComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(IntroductiongroupfilesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
