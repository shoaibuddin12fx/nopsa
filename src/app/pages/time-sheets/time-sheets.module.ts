import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { TimeSheetsPageRoutingModule } from './time-sheets-routing.module';

import { TimeSheetsPage } from './time-sheets.page';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    TimeSheetsPageRoutingModule,
    TranslateModule
  ],
  declarations: [TimeSheetsPage]
})
export class TimeSheetsPageModule {}
