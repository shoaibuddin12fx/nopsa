import { Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';
import { AlertsService } from 'src/app/services/basic/alerts.service';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { BasePage } from '../base-page/base-page';
import { CustomersComponent } from './Components/customers/customers.component';
import { WeatherComponent } from './Components/weather/weather.component';
import { UserSelectionComponent } from './Components/user-selection/user-selection.component';
import { ProjectsService } from 'src/app/services/projects.service';
import { LoadingService } from 'src/app/services/loading-service';
@Component({
  selector: 'app-site-diary',
  templateUrl: './site-diary.page.html',
  styleUrls: ['./site-diary.page.scss'],
})
export class SiteDiaryPage extends BasePage implements OnInit {

  @Input() diaryDetail;
  @Input() editDiary;
  group: FormGroup;
  defaultStartDate: any = new Date().toISOString();
  defaultEndDate: any = new Date().toISOString();
  mimeType;
  image;
  inputFile;
  id;
  statusId;
  statusName;
  weatherVals = [];
  statusList = [];
  projectsList = [];
  weatherList = [];
  weather;
  project;
  submitted = false;
  isUser: boolean = false;
  info;
  selectedWeatherVals;
  weatherDisplayArr = [];
  weatherDisplay;
  translatedWeatherVals;
  weatherflag;
  startDateFlag;
  endDateFlag;
  status;
  loading = false;
  constructor(public injector: Injector, public bobjectsService: BobjectsService, public projectsService: ProjectsService, public loaderService: LoadingService) {
    super(injector)
  }

  ngOnInit() {
    this.createForm();
    this.initalize()
  }


  createForm() {
    this.group = this.formBuilder.group({
      projectId: [null, [Validators.required]],
      startDate: [null, [Validators.required]],
      endDate: [null, [Validators.required]],
      temperatureMorning: [null, [Validators.required]],
      temperatureAfternoon: [null, [Validators.required]],
      description: [null, [Validators.required]],
      contractorReminders: [null, [Validators.required]],
      constructorReminders: [null, [Validators.required]],
      inspections: [null, [Validators.required]],
      othersText: [null, [Validators.required]],
      statusId: [null, [Validators.required]],
      userId: [null, [Validators.required]],
      customers: [null, [Validators.required]],
      sitePower: [null, [Validators.required]],
      fileExist: [null]
    });
  }

  back() {
    this.modals.dismiss();
  }

  async initalize() {
    if (this.editDiary) {
      this.retrieveEditValues(this.diaryDetail);
    }
    else {
      this.projectsList = await this.projectsService.getProjects();
      this.setUser();
      this.info = await this.getDefaultSiteLogInfo();
      this.project = this.info[0].projectId + ',' + this.info[0].projectName;
      console.log(this.info);

      this.defaultStartDate = this.setDefaultCurrentDate().defaultStart;
      this.defaultEndDate = this.setDefaultCurrentDate().defaultEnd;
      this.getStatusList()
    }
  }

  async goToUserSelectionList() {
    const res = await this.modals.present(UserSelectionComponent);
    const data = res.data;
    this.id = data.id;
    if (data.data != 'A' && data) {
      // here you will have a user 
      this.group.controls['userId'].setValue(data.id + ' , ' + data.firstname);
      // set that value in input and formControlName
    }
  }

  async goToCustomerSelectionList() {
    const res = await this.modals.present(CustomersComponent);
    const data = res.data;
    if (data && data.id != undefined || data.firstname != undefined) {
      this.group.controls['customers'].setValue(data.id + ' , ' + data.username);
    }
  }

  async goToWeatherSelectionList() {
    this.weatherflag = true;
    const res = await this.modals.present(WeatherComponent)
    const data = res.data;
    console.log(data);
    if (data) {
      this.weatherflag = false;
    }
    this.weatherVals = data;

    if (this.editDiary) {

    }
    console.log(this.weatherVals);
    this.selectedWeatherVals = this.weatherVals.filter((val) => val.isChecked == true);
    console.log(this.selectedWeatherVals);
    this.getSelectedVals(this.selectedWeatherVals);
  }


  async getStatusList() {
    const bObj = await this.bobjectsService.prepareObject();
    this.network.getStatuses(bObj).then((res) => {
      this.statusList = res['entities'];
      console.log(this.statusList);
    }).catch(err => this.utility.presentFailureToast("Failure"))
  }

  async getProjectsList() {
    this.projectsList = await this.projectsService.getProjects();
    // this.project = this.projectsList[0].name;
    console.log(this.projectsList);
  }


  async editUser(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const bObj = await this.bobjectsService.prepareObject();
      this.customizeFormValues();
      this.setWeatherObject(this.weatherVals);
      const merged = {
        entity: {
          ...this.group.value,
          ...this.weather
        },
        ...bObj,
      }
      this.network.editUser(merged).then(data => {
        console.log(data);
        this.getToastInDifferentLang('success');
        resolve(data);        
      }).catch(err => reject(err));
    })
  }


  async updateUser(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const bObj = await this.bobjectsService.prepareObject();
      const merged = {
        entity: {
          ...this.group.value,
          ...this.weather,
          id: this.diaryDetail["id"]
        },
        ...bObj,
      }
      this.network.editUser(merged).then(data => {
        this.getToastInDifferentLang('success');
        resolve(data);
      }).catch(err => {
        this.getToastInDifferentLang('failure');
        reject(err)
      });
    })
  }

  onSubmit() {
    this.submitted = true;
    this.compareDates(this.group.controls['startDate'].value, this.group.controls['endDate'].value);
    if (this.group.valid && this.startDateFlag == false && this.endDateFlag == false) {
      // this.events.publish('site_diary created',
      //   { data: this.group.controls['projectId'].value?.split(',')[1] }
      // )
      if (this.editDiary) {
        this.updateUser().then(() => {
          console.log("points-taken", this.group.controls['projectId'].value?.split(',')[0])
           this.events.publish('site_diary created',
           { data: this.group.controls['projectId'].value?.split(',')[0] })
          this.group.reset();
          this.weatherDisplay = null;
          this.project = null;
          this.modals.dismiss()
          
        })
      } else {
        this.editUser().then(() => {
          console.log("points-taken", this.group.controls['projectId'].value?.split(',')[0])
          this.group.reset();
          this.weatherDisplay = null;
          this.project = null;
          this.modals.dismiss()
        })
      }
    }
  }

  setWeatherObject(weather) {
    this.weather = {
      [weather[0].val]: weather[0].isChecked,
      [weather[1].val]: weather[1].isChecked,
      [weather[2].val]: weather[2].isChecked,
      [weather[3].val]: weather[3].isChecked,
      [weather[3].val]: weather[4].isChecked,
      [weather[5].val]: weather[5].isChecked,
      [weather[6].val]: weather[6].isChecked,
      [weather[7].val]: weather[7].isChecked,
      [weather[8].val]: weather[8].isChecked
    }
    console.log(this.weather);
  }

  customizeFormValues() {
    this.group.controls['userId'].setValue(this.group.controls['userId'].value?.split(',')[0]);
    this.group.controls['customers'].setValue(this.group.controls['customers'].value?.split(',')[0]);
    this.group.controls['projectId'].setValue(this.group.controls['projectId'].value?.split(',')[0]);
    const startDateTimeStamp = new Date(this.group.controls['startDate'].value).getTime();
    const endDateTimeStamp = new Date(this.group.controls['endDate'].value).getTime();
    this.group.controls['startDate'].setValue(startDateTimeStamp);
    this.group.controls['endDate'].setValue(endDateTimeStamp);
  }

  async setUser() {
    const user = await this.bobjectsService.users.getUserData();
    this.group.controls['userId'].setValue(user.id + ' , ' + user.firstname + ' ' + user.lastname);
    this.isUser = true;
  }

  async getToastInDifferentLang(toasttype) {
    const successMessage = await this.translation.getTranslateKey('Site_Diary_Success')
    if (toasttype == 'success') {
      this.utility.alerts.presentSuccessToast(successMessage);
    }
    else {
      this.utility.alerts.presentFailureToast('Error');
    }
  }

  setDefaultCurrentDate() {
    const defaultStart = new Date();

    const defaultEnd = new Date();

    return {
      defaultStart,
      defaultEnd
    };
  }

  getDefaultSiteLogInfo(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      const bObj = await this.bobjectsService.prepareObject();
      const res = await this.network.getDefaultSiteLogInfo(bObj);
      resolve(res['entities']);
      reject(() => this.utility.presentFailureToast("Failure"))
    })
  }

  async getSelectedVals(selectedVals) {
    var self = this;

    var list = [];

    for (var i = 0; i < selectedVals.length; i++) {

      let v = await self.translation.getTranslateKey(selectedVals[i].val);
      list.push(v);
      console.log(list);
    }

    let joined = list.join(', ');
    console.log(joined);
    this.weatherDisplay = joined;
  }


  compareDates(startDate, endDate) {
    const date1 = Date.parse(startDate);
    const date2 = Date.parse(endDate);

    console.log(date1);
    console.log(date2);
    if (date1 > date2) {
      this.startDateFlag = true;
      this.endDateFlag = true
    }
    else {
      this.startDateFlag = false;
      this.endDateFlag = false;
    }
  }

  async retrieveEditValues(detail) {
    this.getStatusList();
    this.setUser();
    this.projectsList = await this.projectsService.getProjects();
    this.project = detail.projectId + ',' + detail.projectName;
    // this.project = detail.projectId+", "+detail.projectName;
    const startDate = new Date(detail.startDate).toISOString()
    const endDate = new Date(detail.endDate).toISOString()
    this.group.controls["startDate"].setValue(startDate);
    this.group.controls["endDate"].setValue(endDate);
    this.group.controls["temperatureMorning"].setValue(detail.temperatureMorning);
    this.group.controls["temperatureAfternoon"].setValue(detail.temperatureAfternoon);
    this.group.controls["description"].setValue(detail.description);
    this.group.controls["contractorReminders"].setValue(detail.contractorReminders);
    this.group.controls["constructorReminders"].setValue(detail.constructorReminders);
    this.group.controls["inspections"].setValue(detail.inspections);
    this.group.controls["othersText"].setValue(detail.othersText);
    this.group.controls["customers"].setValue(detail.customers);
    this.group.controls["sitePower"].setValue(detail.sitePower);
    this.group.controls["statusId"].setValue(detail.statusId);
    this.weatherList = [
      {
        val: "weatherBroken",
        isChecked: detail.weatherBroken
      },
      {
        val: "weatherCloudy",
        isChecked: detail.weatherCloudy
      },
      {
        val: "weatherFreshBreeze",
        isChecked: detail.weatherFreshBreeze
      },
      {
        val: "weatherLightBreeze",
        isChecked: detail.weatherLightBreeze
      },
      {
        val: "weatherRain",
        isChecked: detail.weatherRain
      },
      {
        val: "weatherSnow",
        isChecked: detail.weatherSnow
      },
      {
        val: "weatherSprinkle",
        isChecked: detail.weatherSprinkle
      },
      {
        val: "weatherStrongGale",
        isChecked: detail.weatherStrongGale
      },
      {
        val: "weatherSunny",
        isChecked: detail.weatherSunny
      }
    ]

    this.setWeatherObject(this.weatherList);
    const editWeatherFilteredList = this.weatherList.filter((val) => val.isChecked == true);
    this.getSelectedVals(editWeatherFilteredList);
  }
}