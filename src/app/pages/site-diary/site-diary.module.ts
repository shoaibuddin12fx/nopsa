import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SiteDiaryPageRoutingModule } from './site-diary-routing.module';

import { SiteDiaryPage } from './site-diary.page';
import { UserSelectionComponent } from './Components/user-selection/user-selection.component';
import { WeatherComponent } from './Components/weather/weather.component';
import { CustomersComponent } from './Components/customers/customers.component';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { DiaryListDetailComponent } from './Components/diary-list-detail/diary-list-detail.component';
import { IonicSelectableModule } from 'ionic-selectable';
import { TranslateModule } from '@ngx-translate/core';
import { PictureUploadComponent } from './Components/picture-upload/picture-upload.component';
import { FileListComponent } from './Components/file-list/file-list.component';
import { DiaryDetailOptionsComponent } from './Components/diary-detail-options/diary-detail-options.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SiteDiaryPageRoutingModule,
    ReactiveFormsModule,
    TranslateModule,
    Ng2SearchPipeModule,
    IonicSelectableModule
  ],
  declarations: [
    SiteDiaryPage, 
    UserSelectionComponent, 
    WeatherComponent, 
    CustomersComponent, 
    DiaryListDetailComponent, 
    PictureUploadComponent, 
    FileListComponent,
    DiaryDetailOptionsComponent,
  ]
})
export class SiteDiaryPageModule { }
