import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SiteDiaryListPage } from './site-diary-list.page';

const routes: Routes = [
  {
    path: '',
    component: SiteDiaryListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SiteDiaryListPageRoutingModule {}
