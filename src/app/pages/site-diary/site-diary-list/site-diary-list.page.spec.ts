import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SiteDiaryListPage } from './site-diary-list.page';

describe('SiteDiaryListPage', () => {
  let component: SiteDiaryListPage;
  let fixture: ComponentFixture<SiteDiaryListPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SiteDiaryListPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SiteDiaryListPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
