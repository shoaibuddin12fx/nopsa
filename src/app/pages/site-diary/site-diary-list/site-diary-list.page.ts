import { ChangeDetectorRef, Component, Injector, OnInit } from '@angular/core';
import { IonicSelectableComponent } from 'ionic-selectable';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { ProjectsService } from 'src/app/services/projects.service';
import { BasePage } from '../../base-page/base-page';
import { DiaryListDetailComponent } from '../Components/diary-list-detail/diary-list-detail.component';
import { UserSelectionComponent } from '../Components/user-selection/user-selection.component';
import { SiteDiaryPage } from '../site-diary.page';

@Component({
  selector: 'app-site-diary-list',
  templateUrl: './site-diary-list.page.html',
  styleUrls: ['./site-diary-list.page.scss'],
})
export class SiteDiaryListPage extends BasePage implements OnInit {
  public id: number;
  public name: string;
  ports: any[];
  project: any;
  plist = [];
  diaryList = [];
  projectId;
  term;
  siteDiaryCreated;
  noProjectsFound: string;
  searchText: string;
  cancelText: string;
  constructor(injector: Injector, public bobjectsService: BobjectsService, public projectService:ProjectsService) {
    super(injector)
    this.events.subscribe('site_diary created',(data)=>{
      this.siteDiaryCreated = true;
      this.project = data.data;
      console.log(this.project);
    })
  }

  ngOnInit() {
    this.initialize();
  }


  async initialize() {
    this.cancelText = await this.translation.getTranslateKey("cancelText");
    this.searchText = await this.translation.getTranslateKey("search");
    this.noProjectsFound = await this.translation.getTranslateKey("error_no_projects");
    this.plist = await this.projectService.getProjects();
    this.project = this.plist[0];
    if(this.projectId){
      this.diaryList = await this.getProjectDiaryLogs(this.projectId);
    }
    else{
      this.diaryList = await this.getProjectDiaryLogs(this.project.id);
    }
  }

  back() {
    this.nav.pop();
  }

  async openSiteDiaryDetailPage(item){
    console.log(item);
    const res = await this.modals.present(DiaryListDetailComponent,{detail:item});
  }

  async openCreatePage(){
    const res = await this.modals.present(SiteDiaryPage);
    if(this.projectId){
      this.diaryList = await this.getProjectDiaryLogs(this.projectId);
    }
    else{
      this.diaryList = await this.getProjectDiaryLogs(this.project.id);
    }
  }

  

  getProjectDiaryLogs(id):Promise<any[]>{
    return new Promise(async (resolve,reject) =>{
      const bObj = await this.bobjectsService.prepareObject();
      const idObj = {ownerId:id}
      const merged = {...bObj,...idObj};
      this.network.getLogs(merged).then(res=>{
        console.log(res);
        resolve(res['entities']);
      }).catch(err =>{
        reject(err);
      })
    })
  }

  async projectChange(event: {
    component: IonicSelectableComponent,
    value: any
  }) {
    this.projectId = event.value.id;
    this.diaryList = await this.getProjectDiaryLogs(this.projectId);
    console.log(this.diaryList);
  }
  

}
