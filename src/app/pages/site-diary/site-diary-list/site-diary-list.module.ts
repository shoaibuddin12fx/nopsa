import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SiteDiaryListPageRoutingModule } from './site-diary-list-routing.module';

import { SiteDiaryListPage } from './site-diary-list.page';
import { IonicSelectableModule } from 'ionic-selectable';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { TranslateModule } from '@ngx-translate/core';
import { FileListComponent } from '../Components/file-list/file-list.component';
import { DiaryDetailOptionsComponent } from '../Components/diary-detail-options/diary-detail-options.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SiteDiaryListPageRoutingModule,
    IonicSelectableModule,
    TranslateModule,
    Ng2SearchPipeModule
  ],
  declarations: [SiteDiaryListPage,FileListComponent,DiaryDetailOptionsComponent]
})
export class SiteDiaryListPageModule {}
