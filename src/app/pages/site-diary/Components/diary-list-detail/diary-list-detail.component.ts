import { Component, ElementRef, Injector, OnInit, ViewChild } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PopoversService } from 'src/app/services/basic/popovers.service';
import { DiaryDetailOptionsComponent } from '../diary-detail-options/diary-detail-options.component';
import { PictureUploadComponent } from '../picture-upload/picture-upload.component';
import { SiteDiaryPage } from '../../site-diary.page';
@Component({
  selector: 'app-diary-list-detail',
  templateUrl: './diary-list-detail.component.html',
  styleUrls: ['./diary-list-detail.component.scss'],
})
export class DiaryListDetailComponent extends BasePage implements OnInit {

  detail;
 
  diaryList = [];

  image;

  inputFile;

  mimeType;

  params;
  path;
  description;

  id;
  editDiary = false;
  @ViewChild('fileupload', { static: true }) fileupload: ElementRef<HTMLElement>;
  constructor(injector:Injector,public bobjectsService: BobjectsService,public cameraService:Camera) { 
    super(injector);
  }

  ngOnInit() {
    this.initialize();
  }

  async initialize(){
    this.description = await this.retreiveImageList();
    console.log(this.description);
    // this.path = await this.retreieveImageURL(this.description?.entities);
    console.log(this.path);
    console.log(this.detail);
  }


  openFile() {
    let el: HTMLElement = this.fileupload.nativeElement;
    el.click();
  }


  // selectUploadOptions() {
  //   this.utility.presentInputAlert('Select upload option',
  //     'Please Select the method for upload',
  //     [
  //       {
  //         name: 'radio1',
  //         type: 'radio',
  //         label: 'camera',
  //         value: 'camera',
  //       },

  //       {
  //         name: 'radio2',
  //         type: 'radio',
  //         label: 'gallery',
  //         value: 'gallery',
  //       }
  //     ],
  //     'Confirm',
  //     'Cancel'
  //   ).then(data => {
  //     if (data == 'camera') {
  //       this.openCamera();
  //       this.utility.dismissAlert();
  //     }
  //     else if (data == 'gallery') { 
  //       this.openFile();
  //     }
  //     else {
  //       console.log('Alert Dismissed');
  //     }
  //   })
  // }


  openCamera() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.cameraService.DestinationType.DATA_URL,
      encodingType: this.cameraService.EncodingType.JPEG,
      mediaType: this.cameraService.MediaType.PICTURE,
      correctOrientation: true
    }

    this.cameraService.getPicture(options).then(async (imageData) => {
      this.image = 'data:image/jpeg;base64,'+imageData;
      this.mimeType = 'image/jpeg';
      this.uploadFile();
    })
  }

  onSelectFile(event) {

    console.log(event)
    let file = <File>event.target.files[0];
    if (file) {

      this.inputFile = file;
      this.mimeType = file.type;
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = async (e: any) => {
        this.image = e.target.result;
        console.log(this.image);
        this.uploadFile();
      }
      console.log(file);
    }
  }


  async uploadFile() {

    var obj1 = await this.bobjectsService.prepareObject();

    let obj2 = {
      'description': this.detail.description,
      'type': "SITELOG",
      'id':this.detail.id,
      'base64': this.image
    }

     var obj = { ...obj1, ...obj2 }

    console.log(obj);
    // console.log(formdata);

      var uploaded = await this.network.uploadImage(obj);
      console.log(uploaded);
  }

  retreiveImageList():Promise<any[]>{
    return new Promise(async (resolve,reject)=>{
      var obj1 = await this.bobjectsService.prepareObject();

      var obj2 = {
        type: "SITELOG",
        id:this.detail.id
      }

      const merged = {...obj1,...obj2};

      this.network.getFileList(merged).then((data)=>{
        console.log('Getting data ',{data});
        resolve(data);
        reject(()=>this.utility.presentFailureToast("Failure"))
      })
    })
  }


  // retreieveImageURL(entity):Promise<any[]>{
  //  return new Promise(async (resolve,reject) =>{
  //     var obj = await this.bobjectsService.prepareObject();
  //   //   if(entity.length>1){
  //   //   this.id = entity[entity.length-1].id;
  //   //   this.params = {...obj,id:this.id};
  //   //   this.network.getUploadedImage(this.params).then(data =>{
  //   //     console.log(data);
  //   //     resolve(data.responseData);
  //   //   })
  //   // }
  //     this.id = entity[0].id;
  //     this.params = {...obj,id:this.id};
  //     this.network.getUploadedImage(this.params).then(data =>{
  //       console.log(data);
  //       resolve(data.responseData);
  //       reject(()=>this.utility.presentFailureToast('Failure'))
  //     })
  //  })
  // }

  
dismiss(){
  this.modals.dismiss();
}

async activatePopover(ev){
  const _data = await this.popover.present(DiaryDetailOptionsComponent,ev);

  const data = _data.data;

  if(data){
    let flag = data.param;

    if(flag == "E"){
      this.openEditSiteDiary();
    }

    if(flag == "P"){
      this.takePhotoPage();
    }
    if(flag == "T"){
      this.openCamera();
    }
  }

}

async openEditSiteDiary(){
  this.editDiary = true;
  const res = await this.modals.present(SiteDiaryPage,{diaryDetail:this.detail,editDiary:this.editDiary});

  const data = res.data;
  console.log(data);
}


async takePhotoPage(){
  const res = await this.modals.present(PictureUploadComponent,{detail:this.detail});

  const data = res.data;

  console.log(data);
}
}
