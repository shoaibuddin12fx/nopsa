import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { ModalService } from 'src/app/services/basic/modal.service';
import { BobjectsService } from 'src/app/services/bobjects.service';

@Component({
  selector: 'app-status',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.scss'],
})
export class WeatherComponent extends BasePage implements OnInit {

  weatherList = [
    {
      val:"weatherBroken",
      isChecked: false
    },
    {
		  val:"weatherCloudy",
      isChecked: false
    },
		{
		  val:"weatherFreshBreeze",
      isChecked: false
    },
		{
		  val:"weatherLightBreeze",
      isChecked: false
    },
		{
		  val:"weatherRain",
      isChecked: false
    },
		{
		  val:"weatherSnow",
      isChecked: false
    },
		{
		  val:"weatherSprinkle",
      isChecked: false
    },
		{
		  val:"weatherStrongGale",
      isChecked: false
    },
		{
		  val:"weatherSunny",
      isChecked: false
    }
  ]
  constructor(injector:Injector, public bObjectService:BobjectsService) {
    super(injector);
   }

  ngOnInit() {}

  onWeatherSelect(item){
    this.dismiss(item);
  }

  dismiss(item){
    this.modals.dismiss(item);
  }
}
