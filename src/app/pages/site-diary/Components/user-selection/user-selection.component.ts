import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { ModalService } from 'src/app/services/basic/modal.service';
import { BobjectsService } from 'src/app/services/bobjects.service';

@Component({
  selector: 'app-user-selection',
  templateUrl: './user-selection.component.html',
  styleUrls: ['./user-selection.component.scss'],
})
export class UserSelectionComponent extends BasePage implements OnInit {
  
  plist = [];
  username;
  token;
  locale;
  term;
  constructor(public injector:Injector,public bobjectsService: BobjectsService) { 
    super(injector)
    this.initialize();
  }
  ngOnInit() {
    
  }

  async initialize(){
    this.plist = await this.getUserList();
    console.log(this.plist);
  }

  getUserList(): Promise<any[]>{

    return new Promise( async (resolve,reject) => {

      const bobj = await this.bobjectsService.prepareObject();
      this.network.getUser(bobj).then((res)=>{
        resolve(res['users']);
        reject(()=>this.utility.presentFailureToast("Failure"))
      })
    })
  }

  onUserSelect(item){
    this.dismiss(item);
  }

  dismiss(res){
    this.modals.dismiss(res);
  }

  
}
