import { Component, Injector, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { BasePage } from 'src/app/pages/base-page/base-page';

@Component({
  selector: 'app-diary-detail-options',
  templateUrl: './diary-detail-options.component.html',
  styleUrls: ['./diary-detail-options.component.scss'],
})
export class DiaryDetailOptionsComponent extends BasePage implements OnInit {

  @Input() flag = 'C';
  @Input() customer;
  hidden;

  constructor(injector:Injector,public popoverCtrl: PopoverController) { 
    super(injector);
    this.events.subscribe("Induction Page Open",this.hideEditSiteDiary.bind(this));
  }

  ngOnInit() {}

  hideEditSiteDiary(){
    console.log("recieved")
    this.hidden = true;
    console.log(this.hidden);
  }
  close(param) {
    this.popoverCtrl.dismiss({param: param });
  }

}
