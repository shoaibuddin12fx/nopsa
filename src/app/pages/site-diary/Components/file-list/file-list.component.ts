import { EventEmitter, Component, Input, OnInit, Output, Injector } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { rejects } from 'assert';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { EventsService } from 'src/app/services/events.service';
import { LoadingService } from 'src/app/services/loading-service';
import { NetworkService } from 'src/app/services/network.service';

@Component({
  selector: 'app-file-list',
  templateUrl: './file-list.component.html',
  styleUrls: ['./file-list.component.scss'],
})
export class FileListComponent extends BasePage implements OnInit {


  _item: any;
  _urls: any;
  _userDetails: any;
  params: any;
  path: any;
  noFiles:string;
  paths = [];
  // ids = [];
  checkbox_hidden: boolean = true;
  pathsArray: void[];
  get item(): any {
    return this._item;
  }

  get urls(): any {
    return this._urls;
  }

  get userDetails(): any {
    return this._userDetails;
  }
  @Input() set item(value: any) {
    this._item = value;
    // call image details api here in this component 
    // and get the image and show inside any image tag in html for preview
    this.retrieveFiles(this._item);
    this.initialize();
  }

  @Input() set urls(value: any) {
    this._urls = value;
  }

  @Input() set userDetails(value: any) {
    this._userDetails = value;
  }

  
  @Output('openFile') openFile: EventEmitter<any> = new EventEmitter<any>();

  constructor(injector: Injector, public network: NetworkService, public bobjectsService: BobjectsService, public events: EventsService,public loaderService:LoadingService) {
   
    super(injector);
    this.events.subscribe('checkbox_hidden', () => {
      this.checkbox_hidden = !this.checkbox_hidden;
    })
  }

  ngOnInit() {
    // this.item["checked"] = false;
  }


  async initialize() {
    // this.path = await this.retreieveImageURL(this.item);
    this.noFiles = await this.translation.getTranslateKey("no_files");
  }

  retrieveFiles(items){
    var self = this;
    this.paths = items;
    this.pathsArray = this.paths.map(val =>{
      this.retreieveImageURL(val).then(data =>{
        val["url"] = data;
      })
      val["checked"] = false;
      return val;
    })
    console.log(this.paths);
  }

  sendPictureClickEvent(path) {
    console.log('event published')
    this.events.publish('Picture Clicked', path);
  }

  emitOpenFile(path) {
    console.log(path);
    if (this.checkbox_hidden == true) {
      this.openFile.emit(path)
    }
  }

  async retreieveImageURL(entity): Promise<any> {
    return new Promise(async (resolve, reject) => {
      var obj = await this.bobjectsService.prepareObject();
      console.log(entity)
      this.params = { ...obj, id: entity.id };
      this.network.getUploadedImage(this.params).then(data => {
        resolve(data.responseData);
      }).catch(err =>{
        reject(err);
      })
    })
  }



  addId($event,path) {
    this.events.publish('addIdcheckedOrNot', {item:path})
  }
}
