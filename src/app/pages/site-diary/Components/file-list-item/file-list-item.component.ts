import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-file-list-item',
  templateUrl: './file-list-item.component.html',
  styleUrls: ['./file-list-item.component.scss'],
})
export class FileListItemComponent implements OnInit {

  _url;

  @Input() set url(value: any) {
    this._url = value;
  }

  get urls(): any {
    return this._url;
  } 
  constructor() { }

  ngOnInit() {}

}
