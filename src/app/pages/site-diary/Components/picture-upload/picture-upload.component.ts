import { Component, ElementRef, Injector, Input, OnInit, ViewChild } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PopoversService } from 'src/app/services/basic/popovers.service';
import { DiaryDetailOptionsComponent } from '../diary-detail-options/diary-detail-options.component';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { LoadingService } from 'src/app/services/loading-service';
@Component({
  selector: 'app-picture-upload',
  templateUrl: './picture-upload.component.html',
  styleUrls: ['./picture-upload.component.scss'],
})
export class PictureUploadComponent extends BasePage implements OnInit {

  @Input() detail;

  diaryList = [];

  image;

  inputFile;

  mimeType;

  params;
  path;
  description;

  id;
  checkbox_Show;
  bObj;
  urls = [];
  ids = [];
  uploadFileResponse;
  @ViewChild('fileupload', { static: true }) fileupload: ElementRef<HTMLElement>;
  constructor(injector: Injector, public bobjectsService: BobjectsService, public cameraService: Camera, public photoViewer: PhotoViewer, public loadingService: LoadingService) {
    super(injector);

    this.events.subscribe("addIdcheckedOrNot", (data) => {
      this.addIdToIdsArray(data.item)
    })
  }

  ngOnInit() {
    this.initialize();
  }

  async initialize() {
    this.bObj = await this.bobjectsService.prepareObject();
    this.description = await this.retreiveImageList()
  }


  openFile() {
    let el: HTMLElement = this.fileupload.nativeElement;
    el.click();
  }


  capturePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.cameraService.DestinationType.DATA_URL,
      encodingType: this.cameraService.EncodingType.JPEG,
      mediaType: this.cameraService.MediaType.PICTURE,
      correctOrientation: true
    }

    this.cameraService.getPicture(options).then(async (imageData) => {
      const image = 'data:image/jpeg;base64,' + imageData;
      this.mimeType = 'image/jpeg';
      this.uploadFileResponse = await this.uploadFile(image);
      this.initialize();
    })
  }

  onSelectFile(event) {

    console.log(event)
    let file = <File>event.target.files[0];
    if (file) {

      this.inputFile = file;
      this.mimeType = file.type;
      let reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = async (e: any) => {
        const image = e.target.result;
        console.log(this.image);
        this.uploadFileResponse = await this.uploadFile(image);
        this.initialize();
      }
      console.log(file);
    }
  }

  hideCheckbox() {
    this.events.publish('checkbox_hidden');
  }
  async uploadFile(image): Promise<any> {
    return new Promise(async (resolve, reject) => {
      var obj1 = await this.bobjectsService.prepareObject();

      let obj2 = {
        'description': this.detail.description,
        'type': "SITELOG",
        'id': this.detail.id,
        'base64': image
      }

      var obj = { ...obj1, ...obj2 }

      console.log(obj);
      // console.log(formdata);
      this.network.uploadImage(obj).then(data => {
        this.events.publish('Do Refresh');
        resolve(data);
      }).catch(err => reject(err))
    })

    // else if (!this.path) {
    //   var uploaded = await this.network.uploadImage(obj);
    //   console.log(uploaded);
    // }
  }

  retreiveImageList(): Promise<any> {
    return new Promise(async (resolve, reject) => {
      var obj1 = await this.bobjectsService.prepareObject();

      var obj2 = {
        type: "SITELOG",
        id: this.detail.id
      }

      const merged = { ...obj1, ...obj2 };

      this.network.getFileList(merged).then((data) => {
        console.log('Getting data ', { data });
        resolve(data);
      }).catch(err => {
        this.utility.presentFailureToast("Failed To load files")
        reject(err)
      })
    })
  }

  deleteFiles(ids): Promise<any> {
    return new Promise(async (resolve, reject) => {
      var obj1 = await this.bobjectsService.prepareObject();
      const merged = { ...obj1, ids: ids }
      this.network.removeImage(merged).then(data => {
        resolve(data)
      })
      .then(()=>{
        this.initialize();
      })
      .catch(err => {
        reject(err);
      })
    })
  }

  dismiss() {
    this.modals.dismiss();
  }

  viewFile($event) {
    if ($event.includes('jpeg') || $event.includes('png')) {
      this.photoViewer.show($event);
    }
    else {
      window.location.href = $event;
    }
  }


  addIdToIdsArray(item) {
    if (item.checked == true) {
      this.ids.push(item.id);
    }
    else {
      const index = this.ids.indexOf(item.id);
      this.ids.splice(index, 1);
    }
  }

  doRefresh($event) {
    this.initialize();
    this.hideCheckbox();
    this.checkbox_Show = false;
    $event.target.complete();
  }


  

}
