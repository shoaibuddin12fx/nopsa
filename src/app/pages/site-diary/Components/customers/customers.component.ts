import { Component, Injector, OnInit } from '@angular/core';
import { BasePage } from 'src/app/pages/base-page/base-page';
import { AlertsService } from 'src/app/services/basic/alerts.service';
import { ModalService } from 'src/app/services/basic/modal.service';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
})
export class CustomersComponent extends BasePage implements OnInit {
  

  customerList = [];
  term;
  loading;
  constructor(injector:Injector,public bobjectsService: BobjectsService, public alertService: AlertsService) { 
    super(injector)

    this.initialize();
  }

  ngOnInit() {
  }

  async initialize(){
    await this.getAllCustomers().then((data)=>{
      this.customerList = data;
      this.loading = false;
    })

    console.log(this.customerList);
  }

  getAllCustomers():Promise<any[]>{
    return new Promise(async (resolve,reject)=>{
      const bobj = await this.bobjectsService.prepareObject();
      this.loading = true;
      console.log("Loading:",this.loading);
      this.network.getSiteCustomers(bobj).then(res =>{
        resolve(res['users'])
        reject(()=>this.utility.presentFailureToast("Failure"))
      })
    })
  }
  
  onSelectCustomer(customer){
    this.dismiss(customer);
  }

  dismiss(res){
    this.modals.dismiss(res);
  }


  async openCreateUserAlert() {
    this.alertService.presentInputAlert('Create User',
    '',
      [
        {
          cssClass:'custom-class',
          name: 'text',
          type: 'text',
          placeholder: 'Enter your Email Address',
        }
      ]
    ).then(data => {
      if (data) {
        console.log("Getting data",data)
        // let obj = {
        //   first_name: 'tes23',
        //   last_name: 'ert'
        // }

        // let d = { ...data, ...obj };
        this.createUser(data);
      }
    })
  }

  async createUser(data) {
    const bObj = await this.bobjectsService.prepareObject();
    const merged = { ...bObj, ...data }
    this.network.createUser(merged).then(res => {
      console.log(merged);
    }).catch(()=>this.utility.presentFailureToast("Failure"))
  }
 

  doRefresh(event){
    this.initialize().then(()=>{
      event.target.complete();
    })
  }
}
