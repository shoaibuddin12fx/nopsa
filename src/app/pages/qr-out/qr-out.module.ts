import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { QrOutPageRoutingModule } from './qr-out-routing.module';

import { QrOutPage } from './qr-out.page';
import { SharedModule } from 'src/app/components/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SharedModule,
    QrOutPageRoutingModule
  ],
  declarations: [QrOutPage]
})
export class QrOutPageModule {}
