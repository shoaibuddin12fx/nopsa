import { formatDate } from '@angular/common';
import { Component, Injector, OnInit } from '@angular/core';
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { AlertService } from 'src/app/services/alert-service';
import { AlertsService } from 'src/app/services/basic/alerts.service';
import { BobjectsService } from 'src/app/services/bobjects.service';
import { QrcodeService } from 'src/app/services/qrcode.service';
import { TranslationService } from 'src/app/services/translation-service.service';
import { BasePage } from '../base-page/base-page';

@Component({
  selector: 'app-qr-code',
  templateUrl: './qr-code.page.html',
  styleUrls: ['./qr-code.page.scss'],
})
export class QrCodePage extends BasePage implements OnInit {

  elementType = 'url';
  qrcode_src = 'NOSCAN';
  user;
  projectData;

  isScanSuccess = false;

  codeIn = null;
  
  constructor(injector: Injector, 
    public barcodeScanner: BarcodeScanner,
    public qrcodeService: QrcodeService,
    ) { 
    super(injector);
    this.initialize()
  }

  ngOnInit() {

  }

  async initialize(){
    this.user = await this.users.getUserData();
    console.log(this.user);
    this.openScan();
    
  }



  async openScan(){

    let value = await this.barcodeScan();
    this.qrcode_src = value;
    const obj = await this.qrcodeService.prepareQrcodeObject(value);
    console.log(obj);
    this.callNetworkResponse(obj)

  }

  async qrcodeIn(){
    const obj = await this.qrcodeService.prepareQrcodeObject(this.qrcode_src, true);
    const data = await this.callNetworkResponse(obj);
    this.nav.pop().then( ev => {
      console.log(ev);
      this.storage.getKey('close_after_time_note').then( v => {
        if(v == true){
          console.log(v);
          this.showAlertBeforeCloseTimeNote(this.projectData.inTime).then(()=>{
            this.events.publish("main:closeapp")
          })
        }
      })
      
    });
  }

  async qrcodeOut() {
    const obj = await this.qrcodeService.prepareQrcodeObject(this.qrcode_src, false);
    const data = await this.callNetworkResponse(obj);
    var self = this;
    self.events.publish("main:closeapp")
  }

  callNetworkResponse(obj): Promise<any>{

    return new Promise( resolve => {
      if(this.projectData){
        obj["comments"] = this.projectData["comments"];
      }

      
      this.network.getItsQrcode(obj).then( res => {
        this.isScanSuccess = true;
        console.log(res);
        if(res){
          this.projectData = res;
          this.projectData["comments"] = "";
          
        }

        resolve(this.projectData);
  
      }, err => {
        this.isScanSuccess = true;
        resolve(null)
        
      })
    })
    
  }



  barcodeScan(): Promise<any>{
    return new Promise( async resolve => {
      
      if(this.platform.is('cordova')){
        this.barcodeScanner.scan().then(barcodeData => {
          resolve(barcodeData.text);
        })
      }else{
        resolve(7565);
      }
      
    })
  }



  async showAlertBeforeCloseTimeNote(inTime){
    const time = new Date(inTime);
    const formattedDate = formatDate(time,'dd.MM.yyyy hh:mm','en-US');
    const successMessageStart = await this.translation.getTranslateKey('Success_Message_Start');
 
    console.log(formattedDate);
    const successMessageEnd = await this.translation.getTranslateKey('Success_Message_End');
    const headerTitle = await this.translation.getTranslateKey('Info');
     
    await this.utility.showAlert(`${successMessageStart} ${formattedDate} ${successMessageEnd}`,headerTitle);
 
   }



}
