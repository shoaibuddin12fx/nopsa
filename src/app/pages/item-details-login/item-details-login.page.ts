import { AfterViewInit, Component, Injector, OnInit } from '@angular/core';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { LoginService } from '../../services/login-service';
import { ToastService } from 'src/app/services/toast-service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { BehaviorSubject } from 'rxjs';
import { BasePage } from '../base-page/base-page';

@Component({
    templateUrl: 'item-details-login.page.html',
    styleUrls: ['item-details-login.page.scss'],
    providers: [LoginService]

})
export class ItemDetailsLoginPage extends BasePage implements OnInit, AfterViewInit {

    data = {};
    type: string;
    loginFailure = true;
    private isLoggedIn = new BehaviorSubject<boolean>(false);
    public isLoggedIn$ = this.isLoggedIn.asObservable();
   
    constructor(injector: Injector,
        private service: LoginService) {
        super(injector);    
        
        this.type = this.activatedRoute.snapshot.paramMap.get('type')
        this.service.load(this.service.getAllThemes()[0]).subscribe(d => {
            this.data = d;
        });
        
    }

    ngAfterViewInit(): void {
        
    }

    ngOnInit(){

        

    }

    isType(item) {
        return item === parseInt(this.type, 10);
    }

    // events
    onLogin(params): void {
        localStorage.setItem('login_email_password', JSON.stringify(params) )
        this.service.login(params).then(async data =>{
            console.log(data);

            this.events.publish('user:processdata', data);

            // await this.users.setUserData(data);
            // localStorage.setItem(environment.HAS_LOGGED_IN,"true");
            // localStorage.setItem(environment.TOKEN, data.token);
            // localStorage.setItem(environment.USER_NAME, data.username);
            // localStorage.setItem(environment.ACCOUNT_ID, data.accountId);
            // this.isLoggedIn.next(true);
            // this.nav.push('/home');
            

            // if(data.body.code == 200){
            //     console.log('Login');
            //     localStorage.setItem(environment.HAS_LOGGED_IN,"true");
            //     localStorage.setItem(environment.TOKEN, data.body.responseData.token);
            //     localStorage.setItem(environment.USER_NAME, data.body.responseData.username);
            //     localStorage.setItem(environment.ACCOUNT_ID, data.body.responseData.accountId);
                
            //     this.router.navigateByUrl('/home');
            //     this.isLoggedIn.next(true);
            // } else {
            //     this.toastCtrl.presentToast('Login Failure');
            //     console.log('Login Failure');
            //     this.loginFailure = false;
            //     this.router.navigateByUrl('/');
            // }
            
        },error =>{
            this.loginFailure = false;
            

            // this.nav.setRoot('/');
        });
    }
    onRegister(params): void {
        this.utility.presentToast('onRegister:' + JSON.stringify(params));
    }
    onSkip(): void {
        this.utility.presentToast('onSkip');
    }
    onFacebook(params): void {
        this.utility.presentToast('onFacebook:' + JSON.stringify(params));
    }
    onTwitter(params): void {
        this.utility.presentToast('onTwitter:' + JSON.stringify(params));
    }
    onGoogle(params): void {
        this.utility.presentToast('onGoogle:' + JSON.stringify(params));
    }
    onPinterest(params): void {
        this.utility.presentToast('onPinterest:' + JSON.stringify(params));
    }
}
