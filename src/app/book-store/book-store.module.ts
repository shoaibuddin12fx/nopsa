import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BookStorePageRoutingModule } from './book-store-routing.module';

import { BookStorePage } from './book-store.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    BookStorePageRoutingModule
  ],
  declarations: [BookStorePage]
})
export class BookStorePageModule {}
