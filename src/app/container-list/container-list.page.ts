import { Component, OnInit } from '@angular/core';
import { ContainerService } from '../services/container.service';
import { FormControl } from '@angular/forms';
import { CommonService } from '../services/common.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-container-list',
  templateUrl: './container-list.page.html',
  styleUrls: ['./container-list.page.scss'],
  providers: [ContainerService]
})
export class ContainerListPage implements OnInit {

  data: any;
  tempData: any;
  type: string;
  searchControl: FormControl;
  searching: any = false;
  itemClicked: boolean = false;
  tempArray: any = [];
  
  constructor(private service: ContainerService, 
    private commonService: CommonService,
    private router: Router) {
    this.getContainerDetails();
  }

  getContainerDetails() {
    this.service.getContainerList().subscribe(d => {
      console.log(d);
      this.data = d.responseData.entities;
      this.tempData = this.data
    }, error => {
      console.log('failure' + error);
    });
  }

  ngOnInit() {
  }

  onSearchTerm(searchTerm) {
    let searchVal = searchTerm.detail.value;
    this.data = this.tempData;

    if (searchVal && searchVal.trim() !== '') {

      this.data = this.data.filter(item => {
        return item.containerNo.indexOf(searchVal) > -1;
      });
    }
  }
  containerItemClick(params): void {
    console.log(params);

    if (params.isChecked) {
      this.tempArray.push(params.id);
    } else {
      this.tempArray.splice(this.tempArray.indexOf(params.id), 1);
    }

    if (this.tempArray.length > 0) {
      this.itemClicked = true;
    } else {
      this.itemClicked = false;
    }
  }


  bookNow() {
    this.commonService.addContainerData(this.tempArray);
    this.router.navigateByUrl("book-container");
  }
}
