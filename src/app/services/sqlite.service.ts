import { Injectable } from "@angular/core";
import {
  SQLite,
  SQLiteDatabaseConfig,
  SQLiteObject,
} from "@ionic-native/sqlite/ngx";
import { Platform } from "@ionic/angular";
import { CapacitorSQLite, JsonSQLite } from "@capacitor-community/sqlite";
import { NetworkService } from "./network.service";
import { UtilityService } from "./utility.service";
import { StorageService } from "./storage.service";
import { browserDBInstance } from "./browser-db-instance";
import { error } from "protractor";
import { Capacitor } from "@capacitor/core";
import { AndroidPermissions } from "@ionic-native/android-permissions/ngx";

const menu_json = require("../data/sidemenu.json");

declare var window: any;
const SQL_DB_NAME = "__nopsaSQLite.db";

@Injectable({
  providedIn: "root",
})
export class SqliteService {
  db: any;
  sqlitedb: SQLiteObject;
  batchSqlCmd;
  batchSqlCmdVals;
  config: SQLiteDatabaseConfig = {
    name: "__nopsa.db",
    location: "default",
  };

  public msg = "Sync In Progress ...";

  constructor(
    private storage: StorageService,
    private platform: Platform,
    private androidpermissions: AndroidPermissions
  ) {}

  public initialize() {
    return new Promise(async (resolve) => {
      if (Capacitor.getPlatform() != "web") await this.initializeDatabase();
      resolve(true);
    });
  }

  async initializeDatabase() {
    return new Promise(async (resolve) => {
      await this.platform.ready();
      // initialize database object
      const flag = await this.createDatabase();

      if (!flag) {
        resolve(false);
        return;
      }

      // initialize all tables

      // initialize users table
      await this.initializeUsersTable();
      // initialize the user flags table for screens
      await this.initializeSidemenuTable();
      // initialize users table

      await this.initializeLogTable();

      resolve(true);
    });
  }

  async createDatabase() {
    return new Promise(async (resolve) => {
      if (Capacitor.getPlatform() != "web") {
        var self = this;
        const dbName = SQL_DB_NAME;
        const dbNames = [SQL_DB_NAME];
        const ret = await CapacitorSQLite.checkConnectionsConsistency({
          dbNames: dbNames,
        });
        if (ret.result == false) {
          await CapacitorSQLite.createConnection({ database: dbName });
          await CapacitorSQLite.open({ database: dbName });
          resolve(true);
        } else {
          await CapacitorSQLite.open({ database: dbName });
          resolve(true);
        }
      } else {
        let _db = window.openDatabase(
          SQL_DB_NAME,
          "1.0",
          "DEV",
          5 * 1024 * 1024
        );
        this.msg = "Database initialized";
        resolve(true);
      }
    });
  }

  async initializeUsersTable() {
    return new Promise((resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS users(";
      sql += "id INTEGER PRIMARY KEY, ";
      sql += "accountId INTEGER DEFAULT 0, ";
      sql += "creationDate TEXT, ";
      sql += "email TEXT, ";
      sql += "firstname TEXT, ";
      sql += "industry INTEGER DEFAULT 0, ";
      sql += "lastname TEXT, ";
      sql += "phoneNumber TEXT, ";
      sql += "project TEXT, ";
      sql += "refreshToken TEXT, ";
      sql += "role_id INTEGER, ";
      sql += "role_name TEXT, ";
      sql += "startApi TEXT, ";
      sql += "success TEXT, ";
      sql += "token TEXT, ";
      sql += "userId INTEGER, ";
      sql += "username TEXT, ";
      sql += "active INTEGER DEFAULT 0 ";
      sql += ")";

      this.msg = "Initializing Users ...";
      resolve(this.execute(sql, []));
    });
  }

  async initializeSidemenuTable() {
    return new Promise((resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS sidemenu(";
      sql += "user_id INTEGER, ";
      sql += "title TEXT, ";
      sql += "route TEXT, ";
      sql += "url TEXT, ";
      sql += "theme TEXT, ";
      sql += "icon TEXT, ";
      sql += "listView BOOLEAN DEFAULT false, ";
      sql += "component TEXT, ";
      sql += "singlePage BOOLEAN DEFAULT false, ";
      sql += "toggle BOOLEAN DEFAULT false, ";
      sql += "orderpair INTEGER, ";
      sql += "UNIQUE(user_id, route)";

      sql += ")";

      this.msg = "Initializing User Menu ...";
      resolve(this.execute(sql, []));
    });
  }

  async initializeFlagsTable() {
    return new Promise((resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS user_flags(";
      sql += "id INTEGER PRIMARY KEY, ";
      sql += "dashboard BOOLEAN DEFAULT false, ";
      sql += "sync_contact_from_phone BOOLEAN DEFAULT false, ";
      sql += "sync_contact_from_zuul BOOLEAN DEFAULT false, ";
      sql += "google_map BOOLEAN DEFAULT false, ";
      sql += "active_pass_list BOOLEAN DEFAULT false, ";
      sql += "archive_pass_list_archive BOOLEAN DEFAULT false, ";
      sql += "archive_pass_list_sent BOOLEAN DEFAULT false, ";
      sql += "archive_pass_list_scanned BOOLEAN DEFAULT false, ";
      sql += "sent_pass_list BOOLEAN DEFAULT false, ";
      sql += "pass_details BOOLEAN DEFAULT false, ";
      sql += "notifications BOOLEAN DEFAULT false, ";
      sql += "request_a_pass BOOLEAN DEFAULT false, ";
      sql += "create_new_pass BOOLEAN DEFAULT false )";

      this.msg = "Initializing User Flags ...";
      resolve(this.execute(sql, []));
    });
  }

  public async setUserInDatabase(_user) {
    return new Promise(async (resolve) => {
      // check if user is already present in our local database, if not, create and fetch his data
      // check if user exist in database, if not create it else update it
      var sql = "INSERT OR REPLACE INTO users(";
      sql += "id, ";
      sql += "accountId, ";
      sql += "creationDate, ";
      sql += "email, ";
      sql += "firstname, ";
      sql += "industry, ";
      sql += "lastname, ";
      sql += "phoneNumber, ";
      sql += "project, ";
      sql += "refreshToken, ";
      sql += "role_id, ";
      sql += "role_name, ";
      sql += "startApi, ";
      sql += "userId, ";
      sql += "username ";
      sql += ") ";

      sql += "VALUES (";

      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "? "; // 15
      sql += ")";

      var values = [
        _user.id,
        _user.accountId,
        _user.creationDate,
        _user.email,
        _user.firstname,
        _user.industry,
        _user.lastname,
        _user.phoneNumber,
        _user.project,
        _user.refreshToken,
        _user.role.id,
        _user.role.name,
        _user.startApi,
        _user.userId,
        _user.username,
      ];

      await this.execute(sql, values);

      if (_user.token) {
        let sql3 = "UPDATE users SET active = ?";
        let values3 = [0];
        await this.execute(sql3, values3);

        let sql2 = "UPDATE users SET token = ?, active = ? where id = ?";
        let values2 = [_user.token, 1, _user.id];

        await this.execute(sql2, values2);
      }

      resolve(await this.getActiveUser());
    });
  }

  async initializeLogTable() {
    return new Promise((resolve) => {
      // create statement
      var sql = "CREATE TABLE IF NOT EXISTS logs(";
      sql += "url text, ";
      sql += "type text, ";
      sql += "sent text, ";
      sql += "data text, ";
      sql += "timestamp )";

      this.msg = "Initializing User Flags ...";
      resolve(this.execute(sql, []));
    });
  }

  // LOG QUERY START

  public async setLogInDatabase(log) {
    return new Promise(async (resolve) => {
      // get sidemenu json data from file and dump into sqlite table

      var sql = "INSERT OR REPLACE INTO logs(";

      sql += "url, ";
      sql += "type, ";
      sql += "sent, ";
      sql += "data, ";
      sql += "timestamp ";

      sql += ") ";

      sql += "VALUES (";

      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "? ";
      sql += ")";

      var values = [
        log["url"],
        log["type"],
        log["sent"],
        log["data"],
        log["timestamp"],
      ];

      await this.execute(sql, values);
      resolve(true);
    });
  }

  public async getLogInDatabase() {
    return new Promise(async (resolve) => {
      let sql = "SELECT * FROM logs order by timestamp desc";
      let values = [];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }

  public async clearLogInDatabase() {
    return new Promise(async (resolve) => {
      let sql = "DELETE FROM logs";
      let values = [];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve([]);
      }
    });
  }

  // LOG QUERY ENDS

  public async getUserSidemenu(withtoggle = false) {
    let user_id = await this.getActiveUserId();
    return new Promise(async (resolve) => {
      // check if a user sidemenu exist ... if yes return that one ... else dump a side menu and return it
      let sql = "SELECT * FROM sidemenu where user_id = ?";
      let values = [user_id];

      if (withtoggle) {
        sql += " and toggle = ?";
        values.push(true);
      }

      sql += " order by orderpair asc";

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(this.setSidemenuWithDefaultValues(user_id, withtoggle));
        return;
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve(this.setSidemenuWithDefaultValues(user_id, withtoggle));
      }
    });
  }

  public async setRefreshToken(token, refreshToke) {
    return new Promise(async (resolve) => {
      let sql3 =
        "UPDATE users SET token = ?, refreshToken = ? where active = ?";
      let values3 = [token, refreshToke, 1];
      await this.execute(sql3, values3);

      resolve(true);
    });
  }

  public async setSidemenuWithDefaultValues(user_id, withtoggle = false) {
    return new Promise(async (resolve) => {
      // get sidemenu json data from file and dump into sqlite table
      let sidemenu = menu_json;
      for (var i = 0; i < sidemenu.length; i++) {
        var sql = "INSERT OR REPLACE INTO sidemenu(";
        sql += "user_id, ";
        sql += "title, ";
        sql += "route, ";
        sql += "url, ";
        sql += "theme, ";
        sql += "icon, ";
        sql += "listView, ";
        sql += "component, ";
        sql += "singlePage, ";
        sql += "toggle, ";
        sql += "orderpair ";
        sql += ") ";

        sql += "VALUES (";

        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "?, ";
        sql += "? "; // 11
        sql += ")";

        var values = [
          user_id,
          sidemenu[i]["title"],
          sidemenu[i]["route"],
          sidemenu[i]["url"],
          sidemenu[i]["theme"],
          sidemenu[i]["icon"],
          sidemenu[i]["listView"],
          sidemenu[i]["component"],
          sidemenu[i]["singlePage"],
          sidemenu[i]["toggle"],
          sidemenu[i]["orderpair"],
        ];

        await this.execute(sql, values);
      }

      resolve(this.getSidemenuByUserId(user_id, withtoggle));
    });
  }

  public async setSidemenuItem(menu) {
    return new Promise(async (resolve) => {
      // get sidemenu json data from file and dump into sqlite table

      var sql = "INSERT OR REPLACE INTO sidemenu(";
      sql += "user_id, ";
      sql += "title, ";
      sql += "route, ";
      sql += "url, ";
      sql += "theme, ";
      sql += "icon, ";
      sql += "listView, ";
      sql += "component, ";
      sql += "singlePage, ";
      sql += "toggle, ";
      sql += "orderpair ";
      sql += ") ";

      sql += "VALUES (";

      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "?, ";
      sql += "? "; // 11
      sql += ")";

      var values = [
        menu["user_id"],
        menu["title"],
        menu["route"],
        menu["url"],
        menu["theme"],
        menu["icon"],
        menu["listView"],
        menu["component"],
        menu["singlePage"],
        menu["toggle"],
        menu["orderpair"],
      ];

      await this.execute(sql, values);

      resolve(menu);
    });
  }

  public async getSidemenuByUserId(user_id, withtoggle = false) {
    return new Promise(async (resolve) => {
      let sql = "SELECT * FROM sidemenu where user_id = ?";
      let values = [user_id];

      if (withtoggle) {
        sql += " and toggle = ?";
        values.push(true);
      }

      sql += " order by orderpair asc";

      // let sql = "SELECT * FROM sidemenu where user_id = ? ORDER BY orderpair asc";
      // let values = [id];

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data);
      } else {
        resolve(null);
      }
    });
  }

  public async getCurrentUserAuthorizationToken() {
    return new Promise(async (resolve) => {
      let user_id = await this.getActiveUserId();
      let sql = "SELECT token FROM users where id = ? limit 1";
      let values = [user_id];

      let d = await this.execute(sql, values);
      // this.utility.presentToast(d);
      if (!d) {
        resolve(null);
        return;
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(data[0]["token"]);
      } else {
        resolve(null);
      }
    });
  }

  public async setUserActiveById(id) {
    return new Promise(async (resolve) => {
      let sql3 = "UPDATE users SET active = ?";
      let values3 = [0];
      await this.execute(sql3, values3);

      let sql2 = "UPDATE users SET active = ? where id = ?";
      let values2 = [1, id];
      await this.execute(sql2, values2);

      resolve(this.getUserById(id));
    });
  }

  public async getUserById(id) {
    return new Promise(async (resolve) => {
      let sql = "SELECT * FROM users where id = ?";
      let values = [id];

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        let id = data[0];
        resolve(id);
      } else {
        resolve(null);
      }
    });
  }

  public async getActiveUserId() {
    return new Promise(async (resolve) => {
      let sql = "SELECT id FROM users where active = ?";
      let values = [1];

      let d = await this.execute(sql, values);
      if (!d) {
        resolve(null);
      }
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        let id = data[0]["id"];
        resolve(id);
      } else {
        resolve(null);
      }
    });
  }

  public async getActiveUser() {
    return new Promise(async (resolve) => {
      let sql = "SELECT * FROM users where active = ? ";
      let values = [1];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        var user = data[0];
        resolve(user);
      } else {
        resolve(null);
      }
    });
  }

  setLogout() {
    return new Promise(async (resolve) => {
      let user_id = await this.getActiveUserId();

      let sql = "UPDATE users SET token = ?, active = ? where id = ?";
      let values = [null, 0, user_id];

      let d = await this.execute(sql, values);
      // var data = d as any[];
      const data = this.getRows(d);
      if (data.length > 0) {
        resolve(true);
      } else {
        resolve(false);
      }
    });
  }

  execute(sql, params) {
    return new Promise(async (resolve) => {
      return CapacitorSQLite.query({database:SQL_DB_NAME,statement:sql,values:params})
        .then((response) => {
          resolve(response);
        })
        .catch((err) => {
          console.error(err);
          resolve(null);
          
        });
    });
  }

  private setValue(k, v) {
    return new Promise((resolve) => {
      this.storage.setKey(k, v).then(() => {
        resolve({ k: v });
      });
    });
  }

  private getValue(k): Promise<any> {
    return new Promise((resolve) => {
      this.storage.getKey(k).then((r) => {
        resolve(r);
      });
    });
  }

  private getRows(data) {
    var items = [];
    for (let i = 0; i < data.values.length; i++) {
      let item = data.values[i];

      items.push(item);
    }

    return items;
  }
}
