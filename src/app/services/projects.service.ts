import { Injectable } from '@angular/core';
import { BobjectsService } from './bobjects.service';
import { NetworkService } from './network.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectsService {
  projectsList = [];

  constructor(public network: NetworkService , public bobjectsService: BobjectsService) { }


  getProjects(): Promise<any[]> {
    return new Promise(async resolve => {
      const bObj = await this.bobjectsService.prepareObject();
      this.network.getAllProjects(bObj).then(async (res) => {
        this.projectsList = res.projects.filter(data => data.name != undefined);
        resolve(this.projectsList);
        console.log(this.projectsList);
      })
    })
  }
}
