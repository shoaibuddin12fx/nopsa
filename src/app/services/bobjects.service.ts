import { Injectable } from '@angular/core';
import { StorageService } from './storage.service';
import { TranslationService } from './translation-service.service';
import { UserService } from './user.service';
import { UtilityService } from './utility.service';
import { Platform } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class BobjectsService {

  constructor(
    public users: UserService, 
    public utility: UtilityService, 
    public translation: TranslationService,
    public storage: StorageService,
    public platform: Platform,    
    ) { 

      
    }

    prepareObject(): Promise<any>{

      return new Promise( async resolve => {
        const coords = await this.utility.checkLocations();
        const locale = await this.translation.getLocale();
        const token = await this.users.getUserToken();
        const user = await this.users.getUserData();
        console.log(coords);
        if(!coords){
          this.platform.ready().then( v => {
            
          })
          
        }
  
        const obj = {
          "username": user ? user['username'] : null,
          "refreshToken": user ? user['refreshToken'] : null,
          "token": token,
          "locale": locale,
          "timestamp": Math.floor(Date.now()/1000),
          "latitude":coords['lat'],
          "longitude":coords['lng'],
          "isMobile":true
        };
        
        resolve(obj);
      })
  
    }
}
