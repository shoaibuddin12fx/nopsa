import { Injectable } from '@angular/core';
import { AlertsService } from './basic/alerts.service';
import { GeolocationsService } from './geolocations.service';
import { LoadingService } from './loading-service';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor(public loading: LoadingService, 
    public geolocations: GeolocationsService,
    public alerts: AlertsService,) { }

  showLoader(msg = 'Please wait...') {
    return this.loading.showLoader(msg);
  }

  hideLoader() {
    return this.loading.hideLoader();
  }

  showAlert(msg,header) {
    return this.alerts.showAlert(msg,header);
  }

  presentToast(msg) {
    return this.alerts.presentToast(msg);
  }

  presentSuccessToast(msg) {
    return this.alerts.presentSuccessToast(msg);
  }

  presentFailureToast(msg) {
    return this.alerts.presentFailureToast(msg);
  }

  presentConfirm(okText = 'OK', cancelText = 'Cancel', title = 'Are You Sure?', message = ''): Promise<boolean> {
    return this.alerts.presentConfirm(okText = okText, cancelText = cancelText, title = title, message = message);
  }

  /* Geolocations */

  getCoordsForGeoAddress(address, _default = true) {
    return this.geolocations.getCoordsForGeoAddress(address, _default = true)
  }

  getCoordsViaHTML5Navigator() {
    return this.geolocations.getCoordsViaHTML5Navigator();
  }

  getCurrentLocationCoordinates() {
    return this.geolocations.getCurrentLocationCoordinates();
  }

  checkLocations(){
    return this.geolocations.checkLocations();
  }

  presentInputAlert(title,message,inputs,okText,cancelText){
    return this.alerts.presentInputAlert(title,message,inputs,okText,cancelText);
  }

  dismissAlert(){
    return this.alerts.dismissAlert();
  }


  formatDate(date){
    var d = new Date(date*1000);
    console.log(d, date);
    var month = d.getMonth() + 1;
    var day = d.getDate();
    var year = d.getFullYear();
    var hh = d.getHours();
    var mm = d.getMinutes();

    var formattedDate = [day,month,year].join('.');

    var formattedDateWithTime = [hh,mm].join(':');

    var fullDate = formattedDate +' '+formattedDateWithTime;

    return fullDate;
  }


  mapArrayCheckedOrUnchecked(array,key,condition){
    const mappedArray = array.map(val => {
      val[key] = condition;
      return val;
    })

    return mappedArray;
  }

  filterCheckedOrUnchecked(array,key,condition){
    const filteredArray = array.filter(val => val[key] == condition);

    return filteredArray;
  }


  addValuesInArrayBasedOnSelection(object,condition,array){
    const objArray = array;
    if(object.checked == condition){
      objArray.push(object);
      return objArray 
    }
    else{
      const index = objArray.indexOf(object.id);
      objArray.splice(index, 1);
      return objArray;
    }
  }
  
}
