import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from './storage.service';
const languages = require('./../data/languages.json');32

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  lang = 'en';
  constructor(private translate: TranslateService, public storage: StorageService) { 
    this.setDefaultLanguage()
  }

  getTranslationsList(){
    return languages; 
  }
  
  async setDefaultLanguage(){
    let language = await this.storage.getKey('language') ?? 'en';
    this.lang = language;
    this.translate.setDefaultLang(language);
    this.translate.use(language);
    
  }

  async getTranslateKey(key): Promise<any> {
    return new Promise( resolve => {
      this.translate.get(key).subscribe( v => {
        resolve(v)
      })
    })
  } 

  getLocale(): Promise<any> {
    return new Promise( async resolve => {
      var v = await this.storage.getKey('language');
      v = v ? v : 'en';
      resolve(v)

    })
  } 

  useTranslation(key){
    console.log(key)
    this.storage.setKey('language', key);
    this.lang = key;
    this.translate.use(key);
  }
  

}
