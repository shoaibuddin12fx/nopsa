import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { SqliteService } from './sqlite.service';
import { UtilityService } from './utility.service';
import { Observable, from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { StorageService } from './storage.service';
import { EncryptService } from '../encrypt.service';


@Injectable({
  providedIn: 'root' 
})
export class InterceptorService implements HttpInterceptor  {

  constructor(private storage: StorageService, public utility: UtilityService, public encrypt: EncryptService) {

  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    return from(this.callToken()).pipe(
      switchMap(token => {
        const cloneRequest = this.addSecret(req, token );
        return next.handle(cloneRequest);
      })
    );
    
  }

  callToken(){
    return new Promise( resolve => {
      let token = localStorage.getItem('token');
      resolve(token)
    });
  }

  private addSecret(request: HttpRequest<any>, value: any){
    let v = value ? value : '';
    let obj = {
      Authorization: 'Bearer ' + v
    }
    
    
    obj['Accept'] = 'application/json';
    let cnt = request.headers.get('Content-Type');
    if(cnt == 'application/json'){
      obj['Content-Type'] = request.headers.get('Content-Type');
    }

    console.log(obj);
    const clone = request.clone(
      {
        setHeaders: obj
      }
    );

    return clone;
  }
}
