import { Injectable } from '@angular/core';
import { ApiService } from './api.service';
import { EventsService } from './events.service';
import { NavService } from './nav.service';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class NetworkService {


  login(data) {
    return this.httpPostResponse('login', data, null, true)
  }

  getUser(data, loader = false, error = false) {
    return this.httpPostResponse('users', data, null, loader, error)
  }

  getItsQrcode(data){
    return this.httpPostResponse('tasks/qrcode', data, null, true)
  }

  getItsQrcodeInOut(data){
    return this.httpPostResponse('tasks/inOut', data, null, false)
  }

  getAllProjects(data){
    return this.httpPostResponse('projects/list', data, null,false)
  }

  getAllLitteras(data){
    return this.httpPostResponse('litteras', data, null, true)
  }

  getTokenRefresh(data){
    return this.httpPostResponse('login/refresh', data, null, false)
  }

  getSiteCustomers(data){
    return this.httpPostResponse('sitelogs/users',data, null, true,true)
  }

  getStatuses(data){
    return this.httpPostResponse('sitelogs/statuses',data,null,false)
  }

  uploadImage(data){
    return this.httpPostResponse('file/upload/base',data,null,true,true);
  }

  createUser(data){
    return this.httpPostResponse('users/sitelog/create',data,null,true,true);
  }

  editUser(data){
    return this.httpPostResponse('sitelogs/edit',data,null,true,true);
  }

  getLogs(data){
    return this.httpPostResponse('sitelogs',data,null,true,true);
  }

  getDefaultSiteLogInfo(data){
    return this.httpPostResponse('/sitelogs/latest',data,null,true,true);
  }

  getUploadedImage(data){
    return this.httpPostResponse('file/path',data,null,false);
  }

  getFileList(data){
    return this.httpPostResponse('file/list',data,null,false,false);
  }

  removeImage(data){
    return this.httpPostResponse('file/remove',data,null,true,true);
  }

  getIntroductionGroups(data){
    return this.httpPostResponse('/introductiongroups',data,null,false,false);
  }

  getAccounts(data){
    return this.httpPostResponse('/accounts/tags',data,null,true,true);
  }

  getintroductionGroupAttributes(data){
    return this.httpPostResponse('introductiongroups/attributes',data,null,false);
  }

  getCurrentUsers(data){
    return this.httpPostResponse('users/currently',data,null,true);
  }

  getUserSkills(data){
    return this.httpPostResponse('/users/userattributes/skills',data,null,false);
  }

  createIntroductionForGroup(data){
    return this.httpPostResponse('introduction/groupIntro',data,null);
  }

  getCreatedIntroductionGroups(data){
    return this.httpPostResponse('introductions/list',data,null,false,false);
  }

  createIntroductionForSingleUser(data){
    return this.httpPostResponse('introduction/create',data,null);
  }

  // getImagesForIntroductionGroups(data){
  //   return this.httpPostResponse('')
  // }

  constructor(
    public utility: UtilityService,
    public api: ApiService,
    private events: EventsService,    
  ) {
    // console.log('Hello NetworkProvider Provider');
  }

  httpPostResponse(key, data, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return this.httpResponse('post', key, data, id, showloader, showError, contenttype);
  }

  httpGetResponse(key, id = null, showloader = false, showError = true, contenttype = 'application/json') {
    return this.httpResponse('get', key, {}, id, showloader, showError, contenttype);
  }

  // default 'Content-Type': 'application/json',
  httpResponse(type = 'get', key, data, id = null, showloader = false, showError = true, contenttype = 'application/json'): Promise<any> {

    return new Promise( ( resolve, reject ) => {

      // console.log(key);
      if (showloader == true) {
        this.utility.showLoader();
      }

      const _id = (id) ? '/' + id : '';
      const url = key + _id;

      // console.info(url);
      const seq = (type == 'get') ? this.api.get(url, {}) : this.api.post(url, data);

      seq.subscribe((data: any) => {

        // console.log(data);
        let res = data['responseData']

        if (showloader == true) {
          this.utility.hideLoader();
        }

        console.log(data['code'])
        if (data['code'] != 200) {
          if (showError) {
            const myVar = data['responseData'];
            if (typeof myVar === 'string' || myVar instanceof String){
              this.utility.presentFailureToast(data['responseData']);
            }else{
              if (!data['responseData']){
                this.utility.presentFailureToast("Request Failed");
                reject(null);
                return;
              }
              if(!data['responseData']['feedback']){
                this.utility.presentFailureToast("Request Failed");
                reject(null);
                return;
              }

              let failures = data['responseData']['feedback']['failures'];
              if(failures.length > 0){
                let msg = failures[0]['name'];
                if(msg){
                  this.utility.presentFailureToast(msg);
                }else{
                  this.utility.presentFailureToast("Request Failed");
                }
                
              }else{
                this.utility.presentFailureToast("Request Failed");
              }

              
            }

            
          }
          reject(null);
        } 
        
        if(data['code'] == 401){
          this.events.publish("user:logout");
        }
        else {
          resolve(res);
        }

      }, err => {

        let error = err;

        if (showloader == true) {
          this.utility.hideLoader();
        }

        if (showError) {
          if(data['code'] != 401){
            this.utility.presentFailureToast(error['responseData']);
          }
          
        }

        console.log(err);

        reject(null);

      });

    });

  }

}
