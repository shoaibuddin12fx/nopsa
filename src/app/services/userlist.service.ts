import { Injectable } from '@angular/core';
import { BobjectsService } from './bobjects.service';
import { LoadingService } from './loading-service';
import { NetworkService } from './network.service';

@Injectable({
  providedIn: 'root'
})
export class UserlistService {

  usersList = [];
  constructor(public network: NetworkService , public bobjectsService: BobjectsService,public loadingService:LoadingService) { }


  getUsersList(data):Promise<any[]>{
    return new Promise((resolve,reject)=>{
      this.network.getCurrentUsers(data).then((users)=>{
        console.log(users);
        resolve(users["entities"]);
      }).catch(err=>{
        reject(err)
      })
    })
  }

  getUserAttributes(data):Promise<any[]>{
    return new Promise((resolve, reject)=>{
      this.network.getUserSkills(data).then((skills)=>{
        resolve(skills["entities"]);
      }).catch(err => {
        reject(err);
      })
    })
  }
}
