import { Injectable, Injector } from '@angular/core';
import { BasePage } from '../pages/base-page/base-page';
import { BobjectsService } from './bobjects.service';

@Injectable({
  providedIn: 'root'
})
export class GroupsService extends BasePage {

  constructor(public bObject: BobjectsService, injector: Injector) {
    super(injector);
  }


  getIntroGroups(data): Promise<any[]> {
    return new Promise(async (resolve, reject) => {
      this.network.getIntroductionGroups(data).then((data) => {
        resolve(data["entities"]);
      }).catch(err => reject(err))
    })
  }

  getSelectedGroupAttributes(data): Promise<any[]> {
    return new Promise(async (resolve, reject)=>{
      this.network.getintroductionGroupAttributes(data).then(data =>{
        resolve(data["entities"]);
      }).catch(err => reject(err))
    })
  }

  createIntroductionForSingleUser(singleUserObj): Promise<any> {
    return new Promise((resolve, reject) => {
      this.network.createIntroductionForSingleUser(singleUserObj).then(async (data) => {
        const success = await this.translation.getTranslateKey("saved");
        this.utility.presentSuccessToast(success);
        resolve(data);
      }).catch(async err => {
        const error =await this.translation.getTranslateKey("error");
        this.utility.presentFailureToast(error);
        reject(err);
      })
    })
  }

  createIntroductionForGroup(multipleUserObj): Promise<any> {
    return new Promise((resolve, reject) => {
      this.network.createIntroductionForGroup(multipleUserObj).then(async (data) => {
        const success =await this.translation.getTranslateKey("saved");
        this.utility.presentSuccessToast(success);
        resolve(data);
      }).catch(async err => {
        const error =await this.translation.getTranslateKey("error");
        this.utility.presentFailureToast(error);
        reject(err);
      })
    })
  }
}
