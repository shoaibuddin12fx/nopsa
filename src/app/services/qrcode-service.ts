import { IService } from './IService';
// import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from './app-settings';
import { LoadingService } from './loading-service';
import { environment } from 'src/environments/environment';
import { HttpHeaders, HttpClient } from '@angular/common/http';

import { forkJoin } from 'rxjs';  // RxJS 6 syntax

@Injectable({ providedIn: 'root' })
export class QRCodeService implements IService {

    tempArray: any = [];
    constructor(
        private loadingService: LoadingService,
        private http: HttpClient) { }


    validateQrCode(qrCodeValue): Observable<any> {
        var url = environment.SERVER_URL + "/rest/qrcode";

        var headers = new HttpHeaders({
            'Access-Control-Allow-Origin': '*',
            'Content-Type': 'application/json'
        });

        let request = {
            'username': localStorage.getItem(environment.USER_NAME),
            'token': localStorage.getItem(environment.TOKEN),
            'locale': 'fi',
            'value': qrCodeValue,
            'projectId': localStorage.getItem(environment.PROJECT_CODE)
        }
        return this.http.post(url, request, { headers: headers });

    }

    bookAll(qrCodeMap): Observable<any[]> {
        
        let container, packages, shipments;

        for (let [key, value] of qrCodeMap) {
            if (value == 2) {
                this.tempArray.splice(0,this.tempArray.length)
                console.log(key.toString());
                this.tempArray.push(key);

                container = this.insertContainer('Test', this.tempArray);
            } else if (value == 1) {
                this.tempArray.splice(0,this.tempArray.length)
                this.tempArray.push[key];
                packages = this.insertPackages('Test', this.tempArray);
            } else if (value == 3) {
                this.tempArray.splice(0,this.tempArray.length)
                this.tempArray.push[key];
                shipments = this.insertShipment('Test', this.tempArray);
            }
            return forkJoin([container, packages, shipments]);
        }
    }

    insertContainer(comment, arr:[]) : Observable<any> {
        var url = environment.SERVER_URL+"/rest/containers/confirm";
       
        var headers = new HttpHeaders({
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json'
        });
    
        let request = {
          'username': localStorage.getItem(environment.USER_NAME),
          'token': localStorage.getItem(environment.TOKEN),
          'locale': 'fi',
          'ids':arr,
          'confirmDate' :new Date().getTime(),
          'location':"1",
          'comment': comment,
          }
        return this.http.post(url,request, {headers: headers});
    
      }

      insertPackages(comment, arr:[]) : Observable<any> {
        var url = environment.SERVER_URL+"/rest/packages/confirm";
       
        var headers = new HttpHeaders({
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json'
        });
    
        let request = {
          'username': localStorage.getItem(environment.USER_NAME),
          'token': localStorage.getItem(environment.TOKEN),
          'locale': 'fi',
          'ids':arr,
          'confirmDate' :new Date().getTime(),
          'location':"1",
          'comment': comment,
          }
        return this.http.post(url,request, {headers: headers});
    
      }

      insertShipment(comment, arr:[]) : Observable<any> {
        var url = environment.SERVER_URL+"/rest/shipments/confirm";
       
        var headers = new HttpHeaders({
          'Access-Control-Allow-Origin': '*',
          'Content-Type': 'application/json'
        });
    
        let request = {
          'username': localStorage.getItem(environment.USER_NAME),
          'token': localStorage.getItem(environment.TOKEN),
          'locale': 'fi',
          'ids':arr,
          'confirmDate' :new Date().getTime(),
          'location':"1",
          'comment': comment,
          }
        return this.http.post(url,request, {headers: headers});
    
      }

    getTitle = (): string => 'Scanner';

    getAllThemes = (): Array<any> => {
        return [
            { 'url': 'qrcode/0', 'title': 'Qrcode Scanner', 'theme': 'layout1' },
            { 'url': 'qrcode/1', 'title': 'Qrcode Encode', 'theme': 'layout2' }
        ];
    }

    getDataForTheme = (menuItem: any): any => {
        return this[
            'getDataFor' +
            menuItem.theme.charAt(0).toUpperCase() +
            menuItem.theme.slice(1)
        ]();
    }

    //* Data Set for page 1
    getDataForLayout1() {
        return {
            'toolbarTitle': 'QRCode page'
        }
    }

    //* Data Set for page 2
    getDataForLayout2() {
        return {
            'toolbarTitle': 'QRCode page',
            'title': 'Click To Generate QR Code',
            'label': 'Please insert your text',
            'buttonText': 'Encode Text'
        }
    }

    load(item: any): Observable<any> {
            return new Observable(observer => {
                observer.next(this.getDataForTheme(item));
                observer.complete();
            });
    }
}
