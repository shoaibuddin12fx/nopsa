import { Injectable } from '@angular/core';
import { UtilityService } from './utility.service';
// import { Geolocation } from '@ionic-native/geolocation/ngx';
import { UserService } from './user.service';
import { StorageService } from './storage.service';
import { TranslationService } from './translation-service.service';
import { BobjectsService } from './bobjects.service';

@Injectable({
  providedIn: 'root'
})
export class QrcodeService {

  constructor(public bobject: BobjectsService) { }

  prepareQrcodeObject(code, status = null): Promise<any>{

    return new Promise( async resolve => {
      const bobj = await this.bobject.prepareObject();

      const obj = {
        "qrcode":code,
        "start":status,  // null | true | false
      };

      const merged = {...bobj, ...obj}
      console.log(merged)
      resolve(merged);
    })


    

  }
}
