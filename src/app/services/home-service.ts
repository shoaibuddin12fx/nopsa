// import { AngularFireDatabase } from '@angular/fire/database';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AppSettings } from './app-settings';

@Injectable({ providedIn: 'root' })
export class HomeService {

    constructor() { }

    // Set data for - HOME PAGE
    getData = () => {
        return {
            'toolbarTitle': 'Home',
            'title': 'Material Handling System',
            'subtitle1': 'Software as SAAS',
            'subtitle2': 'Versatile but easy to use',
            'subtitle3': 'Automated shipments reception',
            'subtitle4': 'Scalable',
            'link': '',
            'description': 'The Material Control system is an easy to use solution for managing deliveries. It meets international standards and its usability and functionalities have been developed with the newest technologies.',
            'background': 'assets/imgs/background/22.jpg'
        };
    }

    load(): Observable<any> {
            return new Observable(observer => {
                observer.next(this.getData());
                observer.complete();
            });
    }
}
