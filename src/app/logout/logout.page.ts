import { Component, Injector, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { BasePage } from '../pages/base-page/base-page';

@Component({
  selector: 'app-logout',
  templateUrl: './logout.page.html',
  styleUrls: ['./logout.page.scss'],
})
export class LogoutPage extends BasePage implements OnInit {

  constructor(injector: Injector) { 
    super(injector)
  }

  ngOnInit() {
    this.logout();
  }

  async logout(){
    await this.sqlite.setLogout();
    this.nav.setRoot('/login/0');
  }

}
