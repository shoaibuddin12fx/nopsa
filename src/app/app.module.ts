
import { NgModule, CUSTOM_ELEMENTS_SCHEMA, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

// import { AngularFireModule } from "@angular/fire";
// import { AngularFireDatabaseModule } from "@angular/fire/database";
// import { AngularFireAuthModule } from "@angular/fire/auth";

import { AppSettings } from './services/app-settings';
import { ToastService } from './services/toast-service';
import { LoadingService } from './services/loading-service';

import { HttpModule } from '@angular/http';

import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';

import { IonicStorageModule } from '@ionic/storage';
import { CommonService } from './services/common.service';
import { NgxQRCodeModule } from '@techiediaries/ngx-qrcode';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NativeStorage } from '@ionic-native/native-storage/ngx';
import { TranslationService } from './services/translation-service.service';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ApiService } from './services/api.service';
import { EventsService } from './services/events.service';
import { NetworkService } from './services/network.service';
import { SqliteService } from './services/sqlite.service';
import { StorageService } from './services/storage.service';
import { InterceptorService } from './services/interceptor.service';
// import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GeolocationsService } from './services/geolocations.service';
import { QrcodeService } from './services/qrcode.service';
import { NgxPubSubService } from '@pscoped/ngx-pub-sub';
import { BobjectsService } from './services/bobjects.service';
// import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Ng2SearchPipeModule } from 'ng2-search-filter';
import { AndroidPermissions } from '@ionic-native/android-permissions/ngx';
import { LocationAccuracy } from '@ionic-native/location-accuracy/ngx';
import { SQLite } from '@ionic-native/sqlite/ngx';
import { HeaderMenuComponent } from './components/header-menu/header-menu.component';
import { AppErrorHandler } from './AppErrorHandler';
import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { Camera } from '@ionic-native/camera/ngx';
import { IonicSelectableModule } from 'ionic-selectable';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
@NgModule({
  declarations: [AppComponent, HeaderMenuComponent],
  entryComponents: [],
  imports: [
    // AngularFireModule.initializeApp(AppSettings.FIREBASE_CONFIG),
    // AngularFireDatabaseModule, AngularFireAuthModule,
    BrowserModule, HttpModule, HttpClientModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    NgxQRCodeModule,
    FormsModule,
    ReactiveFormsModule,
    Ng2SearchPipeModule,
    IonicSelectableModule,
    TranslateModule.forRoot({ 
      loader: {  
        provide: TranslateLoader, 
        useFactory: (createTranslateLoader),  
        deps: [HttpClient] 
      } 
    }) // end translate module
  ],
  providers: [
    StatusBar, BarcodeScanner,
    SplashScreen, ToastService, LoadingService,CommonService,
    { provide: HTTP_INTERCEPTORS, useClass: InterceptorService, multi: true },
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: ErrorHandler, useClass: AppErrorHandler },
    // Geolocation,
    LocationAccuracy,
    NgxPubSubService,
    NativeStorage,
    TranslationService,
    EventsService,
    // SQLite,
    // Geolocation,
    SqliteService,
    StorageService,
    ApiService,
    NetworkService,
    AndroidPermissions,
    GeolocationsService,
    QrcodeService,
    BobjectsService,
    FirebaseX,
    Camera,
    PhotoViewer
  ],
  bootstrap: [AppComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppModule { }

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}
