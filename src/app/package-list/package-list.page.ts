import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';
import { PackageService } from '../services/package.service';
import { CommonService } from '../services/common.service';
import { Router } from '@angular/router';
import { PackageDetailsPage } from '../package-details/package-details.page';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-package-list',
  templateUrl: './package-list.page.html',
  styleUrls: ['./package-list.page.scss'],
  providers: [PackageService]
})
export class PackageListPage implements OnInit {

  data: any;
  tempData: any;
  type: string;
  searchControl: FormControl;
  searching: any = false;
  itemClicked: boolean = false;
  tempArray: any = [];

  constructor(private service: PackageService,
    private commonService: CommonService,
    private router: Router,
    public modalCtrl: ModalController) {
    this.getPackageDetails();
  }

  getPackageDetails() {
    this.service.getPackageList().subscribe(d => {
      this.data = d.responseData.entities;
      this.tempData = this.data
    }, error => {
      console.log('failure' + error);
    });
  }

  async addItemClick(params) {
    //console.log('dfdfdf-->'+params);
    //console.log(params);
    
    if(params.isChecked){
        this.tempArray.push(params.id);
    } else {
        this.tempArray.splice(this.tempArray.indexOf(params.id), 1);
    }

    if(this.tempArray.length>0){
        this.itemClicked = true;
    } else {
        this.itemClicked = false;
    }
  }

  async onItemClickFunc(entry) {
    console.log(entry);
    let profileModal = await this.modalCtrl.create(
      {
        component: PackageDetailsPage,
        componentProps:
        {
          "description": entry.description,
          "poNo": entry.poNo,
          "qty": entry.net,
          "containerNo": entry.containerNo,
          "area": entry.area,
          "gross": entry.gross,
          "height": entry.height,
          "length": entry.length,
          "location": entry.location,
          "net": entry.net,
          "packageNo": entry.packageNo,
          "supplierText": entry.supplierText,
          "volume": entry.volume,
          "width": entry.width
        }
      }
    );
    return profileModal.present();
    //}
  }
  ngOnInit() {
  }

  onSearchTerm(searchTerm) {
    let searchVal = searchTerm.detail.value;
    this.data = this.tempData;

    if (searchVal && searchVal.trim() !== '') {

      this.data = this.data.filter(item => {
        return item.packageNo.indexOf(searchVal) > -1;
      });
    }
  }
  
  bookNow(){
    this.commonService.addPackageData(this.tempArray);
    this.router.navigateByUrl("book-package");
  }
}


