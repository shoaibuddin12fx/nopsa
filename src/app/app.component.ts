import { Component } from '@angular/core';
import {ExportService} from './services/export-service';
import { MenuController, Platform } from '@ionic/angular';
import {SplashScreen} from '@capacitor/splash-screen';
import { StatusBar } from '@capacitor/status-bar';
import { MenuService } from './services/menu-service';
import { NavController } from '@ionic/angular';
import { environment } from '../environments/environment';
import { Router } from '@angular/router';
import { LoginService } from './services/login-service';
import { CommonService } from './services/common.service';
import { TranslationService } from './services/translation-service.service';
import { NavService } from './services/nav.service';
import { UserService } from './services/user.service';
import { GeolocationsService } from './services/geolocations.service';
import { EventsService } from './services/events.service';
import { NetworkService } from './services/network.service';
import { BobjectsService } from './services/bobjects.service';
import { SqliteService } from './services/sqlite.service';
import { UtilityService } from './services/utility.service';
import { StorageService } from './services/storage.service';
import { FirebaseX } from "@ionic-native/firebase-x/ngx";
import { App } from '@capacitor/app';
import { Capacitor } from '@capacitor/core';
@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
  providers: [MenuService, ExportService,CommonService]
})
export class AppComponent {
  public appPages = [];
  headerMenuItem = {
    'image': '',
    'title': '',
    'background': ''
  }
  isEnabledRTL: boolean = false;

  constructor(
    private platform: Platform,
    // private statusBar: StatusBar,
    public menuCtrl: MenuController,
    private menuService: MenuService,
    private events: EventsService,
    private exportService: ExportService,
    public network: NetworkService,
    public nav: NavService,
    public users: UserService,
    public bobject: BobjectsService,
    public sqlite: SqliteService,
    private loginService: LoginService,
    public utility: UtilityService,
    public storage: StorageService,
    public translationService: TranslationService,    
    private firebaseX: FirebaseX
  ) {
    this.isEnabledRTL = localStorage.getItem('isEnabledRTL') == "true";
    //console.log(JSON.stringify(exportService.export()));
    this.initializeApp();
    
  }

  async initializeApp() {
    
    this.platform.ready().then( async () => {
      this.translationService.setDefaultLanguage();

      if(Capacitor.getPlatform() != 'web'){
        StatusBar.setOverlaysWebView({overlay:false});
        StatusBar.setBackgroundColor({color:'#000000'});
      }
      
      this.setRTL();
      
      const flag = await this.sqlite.initialize();
      if(!flag){
        this.exitApp();
      }

      
      
      this.translationService.useTranslation('fi');
      this.storage.getKey('close_after_time_note').then( v => {
        if( v == undefined ){
          this.storage.setKey('close_after_time_note', true);
        }
      })
      this.nav.push('splash')
      this.assignEvents();
      this.events.publish('user:tokenrefresh');
      this.events.subscribe('exitapp', this.exitApp.bind(this));
      // if(this.platform.is('cordova')){        
      //   // this.firebaseX.sendCrash();
      //   // crashlytics.logException('my caught exception');
      // }

      
      
    });
  }
  
  setRTL() {
    document.getElementsByTagName('html')[0]
            .setAttribute('dir', this.isEnabledRTL  ? 'rtl': 'ltr');
  }

  async initializeSidemenu(){
    
    this.appPages = await this.menuService.getSqliteSidemenu()
    this.headerMenuItem = this.menuService.getDataForTheme(null)

  }

  exitApp(){
    navigator['app'].exitApp();
  };

  openPage(page) {
    this.nav.push(page.url, {});
  }

  private async getUser() {
    // SplashScreen.hide();
    const data = await this.bobject.prepareObject();
    
    // console.log(data);

    this.network.getUser(data, false).then(async (_data: any) => {

      let users = _data['users'];
      if(users){
        const _user = users;
        // console.log(_user);

        const dbuser = await this.sqlite.setUserActiveById(_user.id);
        if(dbuser){
          // set user active and redirect to home page
          this.nav.push('home');
        }else{
          this.logout();
        }

        // this.processUserData(_user);
      }
      else {
        // redirect to steps
        this.logout()
      }
    }, err => {
      this.logout()
    })
  }

  private async processUserData(user) {

    // check if sqlite set already, if not fetch records
    var _user = user;

    // const fcm_token = await this.getFCMToken();
    // _user['fcm_token'] = fcm_token;
    // this.user_role_id = parseInt(_user['role_id']);
    // this.utilityProvider.setKey('user_role_id', this.user_role_id);
    _user.id = _user.userId;

    let saveduser = await this.sqlite.setUserInDatabase(_user);
    this.menuCtrl.enable(true, 'authenticated');

    if (!saveduser) {
      this.logout();
      return;
    }

    // console.log(saveduser);
    this.users.setUser(saveduser);
    // this.canBeResident = (parseInt(saveduser["can_user_become_resident"]) == 1);
    // this.canShowSettings = parseInt(saveduser["role_id"]) != 7

    // let currentUrl= this.nav.router.url;
    // console.log(currentUrl);

    // if(currentUrl == '/1/DashboardPage'){
    //   this.events.publish('dashboard:initialize');
    // }else{
    this.nav.setRoot('/home');
    // }


    

  }

  logout() {
    this.menuCtrl.enable(false, 'authenticated');
    this.sqlite.setLogout();
    this.nav.setRoot('/login/0');
  }

  closeApp(){
    console.log("close app here");
    App.exitApp();
  }

  async tokenRefresh(){


    setInterval( async () => {

      let activeUser = await this.sqlite.getActiveUser();

      if(activeUser){
        let bobj = await this.bobject.prepareObject();

        this.network.getTokenRefresh(bobj).then( v => {
          console.log(v);
          // set token and refresh token in sqlite
          this.sqlite.setRefreshToken(v.token, v.refreshToken)
    
        })
      }

    }, 2700000)
    // 2700000

    

    
  }

  private assignEvents() {
    this.events.subscribe('user:logout', this.logout.bind(this));
    // this.events.subscribe('user:login', this.login.bind(this));
    this.events.subscribe('user:tokenrefresh', this.tokenRefresh.bind(this)); 
    this.events.subscribe('user:get', this.getUser.bind(this));
    this.events.subscribe('user:processdata', this.processUserData.bind(this));
    this.events.subscribe('user:init_sidemenu', this.initializeSidemenu.bind(this));
    this.events.subscribe('main:closeapp', this.closeApp.bind(this));
    
    // this.events.subscribe('user:successpage', this.setSuccessPage.bind(this));
    // this.events.subscribe('user:settokentoserver', this.setTokenToServer.bind(this));
    // this.events.subscribe('user:shownotificationalert', this.notificationReceivedalert.bind(this));
    // this.events.subscribe('user:setcontactstodatabase', this.setContacts.bind(this));

  }
}
