import { Injectable, ErrorHandler } from '@angular/core';
import * as stacktrace from 'stacktrace-js';
// import { FirebaseX } from "@ionic-native/firebase-x/ngx";
 
 
@Injectable({providedIn: 'root'})
export class AppErrorHandler implements ErrorHandler {
 
  public Tag = "AppErrorHandler"
 
  constructor () {}
 
  handleError(error: any): void {
       
      // do something with the error
      console.log(error);
 
      // stacktrace.get().then(
      //     trace => this.firebase.logError(this.Tag + " Error =>" + error + "Trace =>" + trace)
      // );
  }
 
}