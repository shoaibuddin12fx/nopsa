import { Component, Input, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-header-menu',
  templateUrl: './header-menu.component.html',
  styleUrls: ['./header-menu.component.scss'],
})
export class HeaderMenuComponent implements OnInit {

  @Input() flag = 'R';
  _pid: any;

  get pid(): any{
    return this._pid;
  }

  @Input() set pid(value: any) {
    this._pid = value;
  
  };
  constructor(public popoverCtrl: PopoverController) { }

  ngAfterViewInit(): void {
    this.initialize();
  }

  ngOnInit() {}

  async initialize(){

    console.log(this.flag);
  }

  close(param) {
    this.popoverCtrl.dismiss({pid: this.pid, param: param });
  }

}
